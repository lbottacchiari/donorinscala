#!/usr/bin/env bash
MASTER=hadoop@ec2-54-163-24-173.compute-1.amazonaws.com

#ssh -i ~/.ssh/ec2key.pem hadoop@ec2-54-145-5-171.compute-1.amazonaws.com
#ssh -i ~/.ssh/ec2key.pem $MASTER  'mkdir ~/apps/donor'
scp -i ~/.ssh/ec2key.pem  /Users/lorenzobottacchiari/Development/projects/donorinscala/target/scala-2.11/donorinscala_2.11-1.0.jar $MASTER:~/donor.jar
scp -i ~/.ssh/ec2key.pem  /Users/lorenzobottacchiari/Development/projects/donorinscala/config/log4j.properties $MASTER:~/
ssh -i ~/.ssh/ec2key.pem $MASTER 'sudo cp ~/log4j.properties /usr/lib/spark/conf'