name := "donorinscala"

version := "1.0"

scalaVersion := "2.11.7"

val sparkVersion = "2.2.0"

scalacOptions += "-target:jvm-1.8"

//on the command line:  sbt reload plugins   , sbt update , sbt reloa

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion


)