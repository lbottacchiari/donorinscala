package donor.ml

import donor.Config
import donor.processing.file.FileReader
import org.apache.log4j.Logger
import org.apache.spark.SparkException
import org.apache.spark.ml.classification.{BinaryLogisticRegressionSummary, LogisticRegression, LogisticRegressionModel}
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.feature._
import org.apache.spark.ml.tuning.{CrossValidator, CrossValidatorModel, ParamGridBuilder}
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}

import scala.annotation.tailrec


//Legacy: moved to python scikit learn project DonorNLP. Reason: Gridsearch for cross validation in spark  is not parallelized.
class NlpML(essayElem: String, reader:FileReader, spark: SparkSession) {
  //essayElem can be title, essay, ...

  import spark.implicits._

  val ngramsFile = "NLP/ngrams_" + essayElem

  val primaryFocusAreas = Seq(
    "History & Civics",
    "Applied Learning",
    "Math & Science",
    "Health & Sports",
    "Literacy & Language",
    "Music & The Arts",
    "Special Needs"
  )

  val logger:Logger = Logger.getLogger(this.getClass().getName())
  val dfLogger = new DataFrameLogger(logger,spark)

  def saveNGrams(nlpTokens :DataFrame, stopwords:Array[String]): DataFrame = {
    var ngrams = nlpTokens

    val inputCol = essayElem + "_tokens"
    val stopWordRemover = new StopWordsRemover()
      .setInputCol(inputCol)
      .setOutputCol("1grams")
      .setStopWords(stopwords)

    ngrams = stopWordRemover.transform(ngrams)

    val bigram = new NGram()
      .setN(2).setInputCol(inputCol).setOutputCol("2grams")

    ngrams = bigram.transform(ngrams)

    val assembleNGram = udf((g1:Seq[String],g2:Seq[String]) => g1 ++ g2)
    ngrams = ngrams.withColumn("ngrams",assembleNGram(col("1grams"),col("2grams")))

    val projects = reader.projects.select("projectid", "primary_focus_area", "history_chunck")
    val outcomes = reader.outcomes.select("projectid", "is_exciting")
    ngrams = ngrams.select("projectid", "ngrams").join(projects, Seq("projectid")).join(outcomes, Seq("projectid"),"left")
    ngrams.write.mode("overwrite").save(Config.outputDir + ngramsFile)
    //ngrams.groupBy("primary_focus_area").count().show()
    dfLogger.show(ngrams)
    ngrams
  }

  def loadNGrams = {
    spark.read.load(Config.outputDir + ngramsFile)
  }


  def predictExciting():DataFrame = {
    val nGrams:DataFrame = loadNGrams.persist()  //loadNGrams.sample(false,0.01).persist()  //20*8*3 = 480

    def mergePrediction(acc:DataFrame, prediction:DataFrame) :DataFrame = {
      if (acc == null) prediction
      else acc.union(prediction)
    }

    def predictSingleArea(area:String) = {
      logger.info(s"*******************************PRIMARY FOCUS AREA: $area ************************************")
      val areaData: DataFrame = nGrams.filter($"primary_focus_area" === area)
      val trainAreaData = areaData.filter("history_chunck < 6043")
      val testAreaData = areaData.filter("history_chunck = 6043")
      val areaModel: CrossValidatorModel = evaluateModel(trainAreaData)
      printBestModelParams(areaModel)
      val areaPredition = areaModel.transform(testAreaData)
      areaPredition
    }

    @tailrec
    def predictAreas(acc:DataFrame, areas:Seq[String]) :DataFrame = {
      if (areas.isEmpty)
        acc
      else {
        val prediction = predictSingleArea(areas.head)
        predictAreas(mergePrediction(acc,prediction),areas.tail)
      }
    }

    val prediction = predictAreas(null,primaryFocusAreas)
    prediction.write.mode("overwrite").save(Config.outputDir + "NLP/prediction_by_" + essayElem)
    prediction
  }


  private def  printBestModelParams(cvmodel:CrossValidatorModel) = {
    logger.info("Best model parameters:")
    val bestPipelineModel = cvmodel.bestModel.asInstanceOf[PipelineModel]
    val stages = bestPipelineModel.stages
    val countVectorizerStage = stages(0).asInstanceOf[CountVectorizerModel]
    logger.info(s"Vocab Site: ${countVectorizerStage.getVocabSize}")
    val lrStage = stages(2).asInstanceOf[LogisticRegressionModel]
    logger.info(s"RegParam: ${lrStage.getRegParam}")

    try {
      // Extract the summary from the returned LogisticRegressionModel instance
      //Attention: The summary is not saved with the model on file
      val trainingSummary = lrStage.summary

      // Obtain the metrics useful to judge performance on test data.
      // We cast the summary to a BinaryLogisticRegressionSummary since the problem is a
      // binary classification problem.
      val binarySummary = trainingSummary.asInstanceOf[BinaryLogisticRegressionSummary]

      // Obtain the areaUnderROC.
      logger.info(s"CV areaUnderROC: ${binarySummary.areaUnderROC}")
    } catch  {
      case e:SparkException => logger.info("No BinaryLogisticRegressionSummary available")
    }

  }

  private def evaluateModel(math: DataFrame) = {

    //7574 features for Math
    val countVectorizer = new CountVectorizer()
      .setInputCol("ngrams")
      .setOutputCol("counts")

    val idf = new IDF()
      .setInputCol("counts")
      .setOutputCol("tfidf")

    val lr = new LogisticRegression()
      .setMaxIter(100)
      .setElasticNetParam(1)
      .setFeaturesCol("tfidf")
      .setLabelCol("is_exciting")
      .setStandardization(true)
      .setFamily("binomial")

    val pipeline = new Pipeline()
      .setStages(Array(countVectorizer, idf, lr))


    val paramGrid = new ParamGridBuilder()
      //.addGrid(countVectorizer.vocabSize, Array(10, 30, 100, 300, 900, 1000, 2000, 3000, 5000, 7000))
      //.addGrid(lr.regParam, Array(0.0001, 0.0003, 0.001, 0.003, 0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30, 100))
      .addGrid(lr.regParam,Ranges.exponentialDoubleRange(-5.0,5.0,100)) //100
      .addGrid(countVectorizer.vocabSize, Ranges.exponentialIntRange(1,5,25)) //25
      .build()


    val classification = new BinaryClassificationEvaluator().setLabelCol("is_exciting")

    val cv = new CrossValidator()
      .setEstimator(pipeline)
      .setEvaluator(classification)
      .setEstimatorParamMaps(paramGrid)
      .setNumFolds(5)



    val cvmodel = cv.fit(math)
    //cvmodel.write.overwrite().save(Config.outputDir + "NLP/lr_model")
    cvmodel
  }



  /*
     EXPERIMENTAL CODE FOLLOWING



  def predict():DataFrame = {

    val train_nlpdata =  loadNGrams //savetrainNlpdata//

    val math = train_nlpdata.filter("primary_focus_area = 'Math & Science'")

    val Array(mathTraining, mathTest) = math.randomSplit(Array(0.8, 0.2), seed = 12345)

    //val model = buildBestModel(mathTraining)
    val model: CrossValidatorModel = loadModel //saveModel(mathTraining) //
    printBestModelParams(model)

    val prediction =  model.transform(mathTest)
    val evaluator = new BinaryClassificationEvaluator().setLabelCol("is_exciting")
    println (s"Test areaUnderROC: ${evaluator.evaluate(prediction)}")

    return train_nlpdata

  }

  private def buildBestModel(math: Dataset[Row]) : PipelineModel = {
    //7574
    val countVectorizer = new CountVectorizer()
      .setInputCol("ngrams")
      .setOutputCol("counts")
      .setVocabSize(800)

    val idf = new IDF()
      .setInputCol("counts")
      .setOutputCol("tfidf")

    val lr = new LogisticRegression()
      .setMaxIter(100)
      .setElasticNetParam(1)
      .setFeaturesCol("tfidf")
      .setLabelCol("is_exciting")
      .setStandardization(true)
      .setFamily("binomial")
      .setRegParam(0.003)

    val pipeline = new Pipeline()
      .setStages(Array(countVectorizer, idf, lr))

    val pipelineModel = pipeline.fit(math)

    val stages = pipelineModel.stages
    val lrStage = stages(2).asInstanceOf[LogisticRegressionModel]


    // Extract the summary from the returned LogisticRegressionModel instance
    //Attention: The summary is not saved with the model on file
    val trainingSummary = lrStage.summary

    // Obtain the objective per iteration.
    val objectiveHistory = trainingSummary.objectiveHistory
    println("objectiveHistory:")
    objectiveHistory.foreach(loss => println(loss))

    // Obtain the metrics useful to judge performance on test data.
    // We cast the summary to a BinaryLogisticRegressionSummary since the problem is a
    // binary classification problem.
    val binarySummary = trainingSummary.asInstanceOf[BinaryLogisticRegressionSummary]

    // Obtain the receiver-operating characteristic as a dataframe and areaUnderROC.
    val roc = binarySummary.roc
    roc.show()
    println(s"CV areaUnderROC: ${binarySummary.areaUnderROC}")

    // Set the model threshold to maximize F-Measure
    val fMeasure = binarySummary.fMeasureByThreshold
    val maxFMeasure = fMeasure.select(max("F-Measure")).head().getDouble(0)
    val bestThreshold = fMeasure.where(col("F-Measure") === maxFMeasure)
      .select("threshold").head().getDouble(0)
    lrStage.setThreshold(bestThreshold)
    println(s"Best Threshold: ${bestThreshold}")

    pipelineModel

  }



  private def loadModel: CrossValidatorModel = {
    CrossValidatorModel.load(Config.outputDir + "NLP/lr_model")
  }

*/

}

object Ranges {
  def main(args:Array[String]) = {
    val r1 = exponentialIntRange(1,5,2)
    val r2 = exponentialIntRange(1,5,25)
    val r3 = exponentialDoubleRange(-5,5,2)
    val r4 = exponentialDoubleRange(-5,5,100)
    println(r2.foreach(println _))
  }

  def exponentialDoubleRange(begin:Double, end:Double, length:Int ) : Array[Double] = {
    val step = (end - begin) / length
    val expRange = Range.Double(begin,end+step,step)
    expRange.toArray.map(e => scala.math.pow(10,e))
  }

  def exponentialIntRange(begin:Int, end:Int, length:Int) : Array[Int] = {
    val step = (end - begin).toDouble / length
    val expRange = Range.Double(begin,end+step,step)
    expRange.toArray.map(e => scala.math.pow(10,e).toInt)

  }

}



/*
+-------------------+-----+
| primary_focus_area|count|
+-------------------+-----+
|               null|   25|
|   History & Civics| 5276|
|   Applied Learning| 8257|
|     Math & Science|34911|
|    Health & Sports| 3708|
|Literacy & Language|58303|
|   Music & The Arts|11804|
|      Special Needs| 9045|


Math and Size
+--------------------------------+------------------------------------------------------------+------------------+-----------+-----------------------------------------------+-------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------------------------------------+-----------------------------------------+----------+
|projectid                       |title_tokens                                                |primary_focus_area|is_exciting|filtered                                       |counts                                     |tfidf                                                                                                             |rawPrediction                           |probability                              |prediction|
+--------------------------------+------------------------------------------------------------+------------------+-----------+-----------------------------------------------+-------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------------------------------------+-----------------------------------------+----------+
|01a86d167d5f223d4418745eeb2158e5|[ooogl, for, safeti, goggl]                                 |Math & Science    |0          |[ooogl, safeti, goggl]                         |(900,[424,760],[1.0,1.0])                  |(900,[424,760],[6.610438286922739,7.416063450909374])                                                             |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|01d9ca92d38dd55ba042cb6d8d82eb00|[calcul, enhanc, math, learn, in, the, classroom]           |Math & Science    |0          |[calcul, enhanc, math, learn, classroom]       |(900,[0,2,6,23,148],[1.0,1.0,1.0,1.0,1.0]) |(900,[0,2,6,23,148],[1.9230020075688259,2.426603153949577,2.892723283168915,4.120226584905045,5.490772589056797]) |[2.510110509676548,-2.510110509676548]  |[0.9248475718028152,0.07515242819718483] |0.0       |
|021db93f3253c6b628b26270a242e8e9|[backyard, butterfli]                                       |Math & Science    |0          |[backyard, butterfli]                          |(900,[257],[1.0])                          |(900,[257],[6.041745280836199])                                                                                   |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|02e533d416f56685cffdbf943a4c0e08|[immers, ourselv, in, a, mural, of, learn]                  |Math & Science    |0          |[immers, mural, learn]                         |(900,[2],[1.0])                            |(900,[2],[2.426603153949577])                                                                                     |[2.3233487818120917,-2.3233487818120917]|[0.910792402414757,0.08920759758524292]  |0.0       |
|0643cf9d3a04ca69cd37f88fa94ac18a|[ipad, for, scienc, inquiri]                                |Math & Science    |0          |[ipad, scienc, inquiri]                        |(900,[1,4,186],[1.0,1.0,1.0])              |(900,[1,4,186],[2.221784722761248,2.79342763331365,5.6899012641671325])                                           |[2.3488518545481623,-2.3488518545481623]|[0.9128429235688506,0.08715707643114952] |0.0       |
|06b05d92ccf667ef004914894ebdfae4|[get, a, grip, on, your, tablet]                            |Math & Science    |0          |[get, grip, tablet]                            |(900,[22,89],[1.0,1.0])                    |(900,[22,89],[4.092398702282305,4.931156801121374])                                                               |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|0713fd4f37368bff504433263372c94f|[robot, for, futur, engin]                                  |Math & Science    |0          |[robot, futur, engin]                          |(900,[20,56,71],[1.0,1.0,1.0])             |(900,[20,56,71],[4.03086641059366,4.515165280026222,4.770226434308737])                                           |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|090f7a24cc9301162538c24306626f53|[get, a, better, look, than, the, nake, eye]                |Math & Science    |0          |[get, better, look, nake, eye]                 |(900,[22,163,197,201],[1.0,1.0,1.0,1.0])   |(900,[22,163,197,201],[4.092398702282305,5.577783966046426,5.760105522840381,5.778454661508578])                  |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|0959cecb98c69534918a905de941cc20|[scientist, observ, natur]                                  |Math & Science    |0          |[scientist, observ, natur]                     |(900,[16,313,625],[1.0,1.0,1.0])           |(900,[16,313,625],[3.833868139383773,6.212090646583438,7.1647490226284685])                                       |[2.2515803057236425,-2.2515803057236425]|[0.9047867618826998,0.09521323811730018] |0.0       |
|0a17b3df6d103edd35ba2f2f20f66b2e|[scienc, equip]                                             |Math & Science    |0          |[scienc, equip]                                |(900,[1,235],[1.0,1.0])                    |(900,[1,235],[2.221784722761248,5.917291106362794])                                                               |[2.193739252111787,-2.193739252111787]  |[0.8996858831279958,0.10031411687200427] |0.0       |
|0c6b3608a5485d995e5e1a117b863f51|[remedi, math, for, older, alaskan, student]                |Math & Science    |1          |[remedi, math, older, alaskan, student]        |(900,[0,7],[1.0,1.0])                      |(900,[0,7],[1.9230020075688259,2.9920726171364604])                                                               |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|0c8798bc974127f6db2d08bb6a7990de|[ap, environment, scienc, start, up]                        |Math & Science    |0          |[ap, environment, scienc, start]               |(900,[1,33,76,110],[1.0,1.0,1.0,1.0])      |(900,[1,33,76,110],[2.221784722761248,4.2785009819161655,4.777006121294116,5.208312460586168])                    |[2.7837089446900665,-2.7837089446900665]|[0.9417891111264861,0.058210888873513826]|0.0       |
|0f698855e3505b11bf6c94e8363d9978|[help, protect, our, tablet]                                |Math & Science    |0          |[help, protect, tablet]                        |(900,[8,89,341],[1.0,1.0,1.0])             |(900,[8,89,341],[3.0286939718249974,4.931156801121374,6.333451503587706])                                         |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|145f14c1207c1f2c90c6fdb8ac061a46|[use, technolog, to, move, us]                              |Math & Science    |0          |[use, technolog, move, us]                     |(900,[3,13,37,210],[1.0,1.0,1.0,1.0])      |(900,[3,13,37,210],[2.5020089848188993,3.7415727342475376,4.318548483045441,5.825856900403162])                   |[2.3366526916539376,-2.3366526916539376]|[0.9118674486559938,0.08813255134400626] |0.0       |
|151e3e2855fb39725b88195d487d3afb|[young, scientist, and, mathematician, in, need, of, suppli]|Math & Science    |0          |[young, scientist, mathematician, need, suppli]|(900,[5,12,16,31,96],[1.0,1.0,1.0,1.0,1.0])|(900,[5,12,16,31,96],[2.7990588072742804,3.7476296879557274,3.833868139383773,4.25400996190787,5.035635871151395])|[2.172112854876297,-2.172112854876297]  |[0.8977171342653788,0.10228286573462128] |0.0       |
|15dd32aec288d1e9b121cda71fed866e|[goopi, goggl]                                              |Math & Science    |0          |[goopi, goggl]                                 |(900,[760],[1.0])                          |(900,[760],[7.416063450909374])                                                                                   |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|166c0a07d54bc78adf17ab4df9c76d77|[human, right]                                              |Math & Science    |0          |[human, right]                                 |(900,[277,386],[1.0,1.0])                  |(900,[277,386],[6.1167804667791135,6.471601842068523])                                                            |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|1677b0b430749a8b99d62f4d993c5a5c|[sim, in, our, classroom]                                   |Math & Science    |0          |[sim, classroom]                               |(900,[6,251],[1.0,1.0])                    |(900,[6,251],[2.892723283168915,5.983249074154591])                                                               |[2.573781266735377,-2.573781266735377]  |[0.9291550057784643,0.07084499422153566] |0.0       |
|16bc6343f49921cb8ba69df573582824|[electr, and, current]                                      |Math & Science    |0          |[electr, current]                              |(900,[284,583],[1.0,1.0])                  |(900,[284,583],[6.1298525483464665,7.059388506970642])                                                            |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
|16c6561cabadba82e08cf3dcdc590569|[captiv, by, the, common, core]                             |Math & Science    |1          |[captiv, common, core]                         |(900,[35,38],[1.0,1.0])                    |(900,[35,38],[4.288885291221882,4.3359024977385925])                                                              |[2.2766750497181105,-2.2766750497181105]|[0.9069267652913777,0.09307323470862236] |0.0       |
+--------------------------------+------------------------------------------------------------+------------------+-----------+-----------------------------------------------+-------------------------------------------+------------------------------------------------------------------------------------------------------------------+----------------------------------------+-----------------------------------------+----------+
only showing top 20 rows

Vocab Site: 800
RegParam: 0.003
areaUnderROC: 0.593015969120972 (prediction on all training data)
Best Threshold: 0.09391957496176671

With train test split:
on testsample = areaUnderROC: 0.5560575882998712
on trainsample = areaUnderROC: 0.5972876358507148



Vocab Site: 262144
RegParam: 0.0020892961308540386
 */