package donor.ml

import org.apache.spark.ml.Transformer
import org.apache.spark.ml.param.{Param, ParamMap, Params, StringArrayParam}
import org.apache.spark.ml.util.{DefaultParamsWritable, Identifiable}
import org.apache.spark.sql.{Column, DataFrame, Dataset}
import org.apache.spark.sql.types.{StructField, StructType}
import org.apache.spark.sql.functions._

//HasInputCols is private in MLib. I have to reimplement it
trait HasInputCols extends Params {
  final val inputCols = new StringArrayParam(this, "inputCols", "input column names")

  final def getInputCols: Array[String] = $(inputCols)
}


class ColumnSelector(override val uid:String) extends Transformer with HasInputCols with  DefaultParamsWritable {
  def this() = this(Identifiable.randomUID("columnSelector"))

  override def transform(dataset: Dataset[_]): DataFrame = {
    val inputColNames:Array[String] = $(inputCols)
    val columns:Array[Column] = inputColNames.map(name => col(name))
    dataset.select(columns : _*)
  }

  override def copy(extra: ParamMap): ColumnSelector =  defaultCopy(extra)

  override def transformSchema(schema: StructType): StructType = {
    val inputColNames = $(inputCols)
    val structFields:Array[StructField] =  inputColNames.map(col => schema(col))
    new StructType(structFields)
  }

  def setInputCols(value: Array[String]): this.type = set(inputCols, value)
}

class ColumnDropper(override val uid:String) extends Transformer with HasInputCols with  DefaultParamsWritable {
  def this() = this(Identifiable.randomUID("columnDropper"))

  override def transform(dataset: Dataset[_]): DataFrame = {
    val inputColNames:Array[String] = $(inputCols)
    val columnNames:Array[String] = dataset.columns.diff(inputColNames)
    val columns:Array[Column] =  columnNames.map(name => col(name))
    dataset.select(columns : _*)
  }

  override def copy(extra: ParamMap): ColumnDropper =  defaultCopy(extra)

  override def transformSchema(schema: StructType): StructType = {
    val inputColNames = $(inputCols)
    val structFields:Array[StructField] =  schema.fields.filter( field => !inputColNames.contains(field.name))
    new StructType(structFields)
  }

  def setInputCols(value: Array[String]): this.type = set(inputCols, value)
}