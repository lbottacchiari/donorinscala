package donor.ml

import donor.processing.predictor.FG1PredictorFeatures
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.ml.feature.{StringIndexer, VectorAssembler}
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Created by LBottacchiari_ext on 03.07.2017.
  */
class FeatureIndexer (fullset:DataFrame, spark: SparkSession) {
  //"dealine_days" is not included in the train at the moment. should we include it?

  private val model = evaluateFeatureIndexer()

  private def evaluateFeatureIndexer(): PipelineModel = {
    val indexedCategoricalColNames = FG1PredictorFeatures.categoricalFeatures.map(f => "indexed_" + f)

    val stringIndexersCategoricalFeatures = FG1PredictorFeatures.categoricalFeatures.map(f => new StringIndexer().setInputCol(f).setOutputCol("indexed_" + f) )

    val vectorAssembler = new VectorAssembler()
      .setInputCols(indexedCategoricalColNames ++ FG1PredictorFeatures.numericalFeatures)
      .setOutputCol("features")

    val columnDropper = new ColumnDropper()
      .setInputCols(FG1PredictorFeatures.categoricalFeatures ++ indexedCategoricalColNames ++ FG1PredictorFeatures.numericalFeatures)

    val pipeline = new Pipeline().setStages(stringIndexersCategoricalFeatures :+ vectorAssembler :+ columnDropper)
    pipeline.fit(fullset)
  }

  def transform(dataset: DataFrame) : DataFrame = {
    model.transform(dataset)
  }

}
