package donor

/**
  * Created by lorenzobottacchiari on 07/05/17.
  */
object Config {
  def projectDir = evaluateProjectDir()
  def dataDir = projectDir + "data/"
  def inputDir = dataDir + "input/"
  def outputDir = dataDir + "output/"
  def nlpDir = dataDir + "output/NLP/"

  val FG1OutcomesFile = "FG1_Outcomes"
  val FG1PredictorsFile = "FG1_Predictors"
  val FG1Trainset = "FG1_Trainset"
  val FG1Testset = "FG1_Testset"
  val predictionFile = "Prediction"
  val submissionFile = "Submission.csv"
  val FG1TrainsetR = "FG1_Trainset_R.csv"
  val FG1TestsetR = "FG1_Testset_R.csv"
  val wordProxiesFile = "word_proxies"
  val FG1PredWithWordProxies = "FG1_Predictors_WordPx"

  val FG2TrainsetR = "FG2_Trainset"
  val FG2TestsetR = "FG2_Testset"

  val testFile = "Testfile"

  val logfile = outputDir + "donorlog.txt"

  def fileSystem = System.getProperty("donor.fs")

  def evaluateProjectDir():String = {

    fileSystem match {
      case "win" =>  "/Users/LBottacchiari_ext/Development/projects/donorinscala/"
      case "mac" =>  "/Users/lorenzobottacchiari/Development/projects/donorinscala/"
      case "s3n" => "s3n://bytepocket-datascience/donors_choose/" //This can be used to acces s3 from spark on your local mac
      case "s3" => "s3://bytepocket-datascience/donors_choose/" //This can be used to acces s3 from amazon emr 
      case _ =>  "todo"
    }
  }



}
