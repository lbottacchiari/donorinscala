package donor.examples

import org.apache.spark.{SparkConf, SparkContext}

object SimpleApp {
  def main(args: Array[String]) {
    //val logFile = "/Users/lorenzobottacchiari/Development/spark/spark-2.1.0-bin-hadoop2.7/README.md" // Should be some file on your system
    val logFile = "/tmp/College.csv" // Should be some file on your system

    val conf = new SparkConf().setAppName("Simple Application").setMaster("local")
    val sc = new SparkContext(conf)




    val logData = sc.textFile(logFile, 2).cache()
    val numAs = logData.filter(line => line.contains("a")).count()
    val numBs = logData.filter(line => line.contains("b")).count()
    println(s"Lines with a: $numAs, Lines with b: $numBs")
    sc.stop()
  }
}