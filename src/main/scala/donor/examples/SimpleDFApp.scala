package donor.examples


import java.sql.Date
import java.time.{LocalDate, Month}

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Column, SparkSession}
/**
  * Created by lorenzobottacchiari on 26/02/17.
  */
object SimpleDFApp {
  def main(args:Array[String]) = {
    val spark = SparkSession
      .builder()
      .appName("Simple DF App")
      .master("local")
      .getOrCreate()

    // For implicit conversions like converting RDDs to DataFrames
    import spark.implicits._

    val columnNames = Array("projectid","teacherid","date")


    var df = spark.createDataFrame(Seq(
      ("p1","t1",  Date.valueOf(LocalDate.of(2012, Month.SEPTEMBER, 1))),
      ("p2","t1",  Date.valueOf(LocalDate.of(2012, Month.SEPTEMBER, 12))),
      ("p3","t2",  Date.valueOf(LocalDate.of(2012, Month.OCTOBER, 23))),
      ("p4","t2",  Date.valueOf(LocalDate.of(2012, Month.NOVEMBER, 15))),
      ("p5","t2",  Date.valueOf(LocalDate.of(2012, Month.NOVEMBER, 18))),
      ("p6","t3",  Date.valueOf(LocalDate.of(2012, Month.DECEMBER, 12)))
    )).toDF(columnNames:_*)


    def lapse(pos:Int):Column =  datediff( $"date" , lag($"date",pos).over(Window.partitionBy($"teacherid").orderBy($"date")) )


    df.withColumn("lag", lapse(1)) .show()


    /*
    val data = Seq(
      ("row_1", "item_1"),
      ("row_2", "item_1"),
      ("row_3", "item_2"),
      ("row_4", "item_1"),
      ("row_5", "item_2"),
      ("row_6", "item_1")).
      toDF("col1", "col2")

    data.
      select(
        $"*",
        coalesce(lead($"col2", 1).over(Window.orderBy($"col1")), $"col2").as("col2_next")
      ).show()
    */



  }
}
