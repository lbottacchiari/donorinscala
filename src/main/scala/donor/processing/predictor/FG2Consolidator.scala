package donor.processing.predictor

import donor.Config
import donor.DonorMain.spark
import donor.processing.file.FileReader
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}

//See FG2_conso.R and FG2_subset.R in KDD Robot R Solution

class FG2Consolidator (reader:FileReader, aggregator: Aggregator, teacherHistory: TeacherHistory, locationHistory: LocationHistory, spark: SparkSession) {
  import spark.implicits._

  val stacked_prediction_files = Seq(
    "is_exciting_title_tokens_stacked_prediction.csv",
    "is_exciting_essay_tokens_stacked_prediction.csv",
    "great_messages_proportion_essay_tokens_stacked_prediction.csv",
    "teacher_referred_count_essay_tokens_stacked_prediction.csv",
    "non_teacher_referred_count_essay_tokens_stacked_prediction.csv",
    "t_1p_essay_tokens_stacked_prediction.csv",
    "green_essay_tokens_stacked_prediction.csv",
    "great_chat_essay_tokens_stacked_prediction.csv",
    "nt_3p_essay_tokens_stacked_prediction.csv",
    "nt_100p_essay_tokens_stacked_prediction.csv",
    "thoughtful_donor_essay_tokens_stacked_prediction.csv",
    "is_exciting_item_tokens_stacked_prediction.csv"
  )

  val stacked_feature_name = Seq(
    "is_exciting_title_tokens_prediction",
    "is_exciting_essay_tokens_prediction",
    "great_messages_proportion_essay_tokens_prediction",
    "teacher_referred_count_essay_tokens_prediction",
    "non_teacher_referred_count_essay_tokens_prediction",
    "t_1p_essay_tokens_prediction",
    "green_essay_tokens_prediction",
    "great_chat_essay_tokens_prediction",
    "nt_3p_essay_tokens_prediction",
    "nt_100p_essay_tokens_prediction",
    "thoughtful_donor_essay_tokens_prediction",
    "is_exciting_item_tokens_prediction"
  )

  val feature_subset_from_FG1 = Seq(
    "projectid",
    "secondary_focus_subject",
    "primary_focus_subject",
    "school_latitude",
    "cost",
    "school_longitude",
    "Vol_teacher_acctid",
    "resource_type",
    "teacher_teach_for_america",
    "Vol_school_city",
    "school_zip",
    "school_county",
    "students_reached",
    "school_city",
    "Vol_schoolid",
    "Vol_school_county",
    "school_district",
    "Vol_school_zip",
    "teacher_prefix",
    "Vol_school_district",
    "schoolid",
    "school_metro",
    "school_state",
    "eligible_double_your_impact_match",
    "Vol_school_state",
    "Vol_school_ncesid",
    "school_charter",
    "teacher_acctid",
    "eligible_almost_home_match",
    "grade_level",
    "poverty_level",
    "school_nlns",
    "school_kipp",
    "school_magnet",
    "school_charter_ready_promise",
    "teacher_ny_teaching_fellow",
    "school_year_round",
    "lapse",
    "lapse2",
    "lapse3",
    "H1_sum_gexciting",
    //missing in FG1,probably forgotten:  "H1_sum_g100_and_up",
    "H1_sum_g10_to_100",
    "H1_sum_gunder_10",
    "history_chunck" //Needed?
  )

  val word_proxies_features = Seq (
    "essayNbOfChars",
    "essayStringMax",
    "essayNbWords",
    "essayNbUniqueWord",
    "essayRUniqueWords",
    "essayWordsLengthQ90",
    "essayWordsLengthQ95",
    "essayWordsLengthQ99",
    "essayNbSentences",
    "essayWordsMeanInSent",
    "essayDigit",
    "essayPunct",
    "essayQuotM",
    "essaySlash",
    "essayEmark",
    "essayColon",
    "essayQuestion",
    "essayBracket",
    "essayComma",
    "essaySemicolon",
    "essaypTransW",
    "essayPPrV",
    "essayPErr"
  )





  val feature_subset_from_FG1_with_word_proxies = feature_subset_from_FG1.union(word_proxies_features)


  val cost_export_file = "costdata"
  val cost_stacked_prediction_file = "cost_stacked_prediction.csv"
  val cost_feature_name = "cost_prediction"


  def consolidate() :DataFrame = {
    val stackedPredictions = mergeStackedPredictions(None,stacked_prediction_files, stacked_feature_name ).get
    val vendorId = aggregator.mostExpensiveItem.select("projectid","vendorid").na.fill("UNKNOWN",Array("vendorid"))
    val donationLocationHist = teacherHistory.donationLocationHistoricalFeatures.drop("").drop("teacher_acctid").drop("history_chunck")
    val gaveLocationHist = teacherHistory.gaveLocationHistoricalFeatures.drop("teacher_acctid").drop("history_chunck")
    val zipHist = locationHistory.zipHistoricalFeatures.drop("school_zip").drop("history_chunck")
    val stateHist = locationHistory.stateHistoricalFeatures.drop("school_state").drop("history_chunck")
    val cityHist = locationHistory.cityHistoricalFeatures.drop("school_city").drop("history_chunck")
    //add subset of features from FG1
    val FG1FeatureSubset = spark.read.load(Config.outputDir + Config.FG1PredWithWordProxies).select(feature_subset_from_FG1_with_word_proxies.head, feature_subset_from_FG1.union(word_proxies_features).tail:_*)


    stackedPredictions
      .join(vendorId,Seq("projectid"),"left")
      .join(costDeviation(), Seq("projectid"),"left")
      .join(donationLocationHist,Seq("projectid"),"left")
      .join(gaveLocationHist,Seq("projectid"),"left")
      .join(zipHist,Seq("projectid"),"left" )
      .join(stateHist, Seq("projectid"),"left")
      .join(cityHist, Seq("projectid"),"left")
      .join(FG1FeatureSubset,Seq("projectid"),"left")
  }


  /* Deviation of current cost from cost stacked predicted
     Needed files i output folder:
     1. Export cost data to file 'costdata' (DonorMain with argument -cost.data)
     2. Make stacked predictions of cost in python and generate file cost_stacked_prediction.csv'
   */
  def costDeviation(): DataFrame = {

    val costExportDf = spark.read.load(Config.outputDir + cost_export_file)

    val costPredictionFields = List(
      StructField("projectid", StringType, false),
      StructField(cost_feature_name, DoubleType, false)
    )

    val costPredictionDf = spark.read
      .option("header", true)
      .schema(StructType(costPredictionFields))
      .csv(Config.outputDir + cost_stacked_prediction_file)

    costExportDf.join(costPredictionDf, Seq("projectid"))
      .withColumn("cost_deviation", $"cost"/$"cost_prediction")
      .select("projectid","cost_deviation","cost_prediction")  //cost_prediction is named Normal_cost in R solution
  }


  def mergeStackedPredictions(acc: Option[DataFrame] , fileNames: Seq[String], featureNames: Seq[String]): Option[DataFrame] = {
    if (fileNames.isEmpty)
      acc
    else {
      val fileName = Config.nlpDir + fileNames.head
      val featureName = featureNames.head

      val featureFields = List(
        StructField("projectid", StringType, false),
        StructField(featureName, DoubleType, false)
      )

      val featureDf = spark.read
        .option("header", true)
        .schema(StructType(featureFields))
        .csv(fileName)

      val newAcc = if (acc.isEmpty) Some(featureDf) else Some(acc.get.join(featureDf,Seq("projectid")))
      mergeStackedPredictions(newAcc, fileNames.tail, featureNames.tail)

    }
  }


  def testFG2LoadFeatures(): Unit = {
    val FG1FeatureSubset = spark.read.load(Config.outputDir + Config.FG1PredWithWordProxies).select(feature_subset_from_FG1_with_word_proxies.head, feature_subset_from_FG1.union(word_proxies_features).tail:_*)
    FG1FeatureSubset.show()
    FG1FeatureSubset.printSchema()

  }
}




object  FG2Consolidator {
  def main(args: Array[String]) = {
    //new FG2Consolidator(null,null,null,null, null).stacked_feature_name.foreach(f => print("\""+f+"\",\"vendorid\""))
    val sa: Array[String] = Array("pippo","franco")

    val c = sa.map(s => "\""+s+"\"").mkString(",")       //foreach(f => print("\""+f+"\","))
    println(c)
    print("donor_zip".replace("donor","school"))

  }
}


/*
FG2 features from KDD R Solution
C_subset<-c(
  ** FROM FG1
  "secondary_focus_subject","primary_focus_subject","school_latitude",
  "cost","school_longitude","Vol_teacher_acctid",
  "resource_type","teacher_teach_for_america","Vol_school_city",
  "school_zip","school_county","students_reached",
  "school_city","Vol_schoolid","Vol_school_county",
  "school_district","Vol_school_zip","teacher_prefix",
  "Vol_school_district","schoolid","school_metro",
  "school_state","eligible_double_your_impact_match","Vol_school_state",
  "Vol_school_ncesid","school_charter","teacher_acctid",
  "eligible_almost_home_match","grade_level","poverty_level",
  "school_nlns","school_kipp","school_magnet",
  "school_charter_ready_promise","teacher_ny_teaching_fellow","school_year_round",
  "essaystring_max","essayNbWords","essayUniqueWords",
  "essayRUniqueWords","essaynchar","essayWordsLengthQ90",
  "essayWordsLengthQ95","essayWordsLengthQ99","essayNbSentences",
  "essayWordsMeanInSent","essayDigit","essayPunct",
  "essayQuotM","essaySlash","essayEmark",
  "essayColon","essayQuestion","essayBraket",
  "essayComma","essaySemiColon","essayPTransW",
  "essayPPrV","essayPErr","lapse",
  "lapse2","lapse3","lapse_m1",
  "H1_sum_gexciting","H1_sum_g100_and_up","H1_sum_g10_to_100",
  "H1_sum_gunder_10","H1_sum_sponsored_distance",

  **New in FG2
  "title", "essay","essay_great_messages_proportion","essay_teacher_referred_count",
  "essay_non_teacher_referred_count","essay_t_1p_num","essay_green_num",
  "essay_great_chat_num","essay_nt_3p_num","essay_nt_100p_num",
  "essay_thoughtful_donor_num",

  "Normal_Cost","Cost_Deviation",

  "vendorid","item_exciting",

  "history_chunck",

  "donor_city_H1_nb_donations","donor_city_H1_sum_gave_2_exciting","donor_city_H1_sum_donation_total",
  "donor_city_H1_mean_gave_2_exciting","donor_city_H1_mean_donation_total","donor_state_H1_nb_donations",
  "donor_state_H1_sum_gave_2_exciting","donor_state_H1_sum_donation_total","donor_state_H1_mean_gave_2_exciting",
  "donor_state_H1_mean_donation_total","donor_zip_H1_nb_donations","donor_zip_H1_sum_gave_2_exciting",
  "donor_zip_H1_sum_donation_total","donor_zip_H1_mean_gave_2_exciting","donor_zip_H1_mean_donation_total"
)





Result
FG2  //TODO to be updated
root
 |-- projectid: string (nullable = true)
 |-- is_exciting_title_tokens_prediction: double (nullable = true)
 |-- is_exciting_essay_tokens_prediction: double (nullable = true)
 |-- great_messages_proportion_essay_tokens_prediction: double (nullable = true)
 |-- teacher_referred_count_essay_tokens_prediction: double (nullable = true)
 |-- non_teacher_referred_count_essay_tokens_prediction: double (nullable = true)
 |-- t_1p_essay_tokens_prediction: double (nullable = true)
 |-- green_essay_tokens_prediction: double (nullable = true)
 |-- great_chat_essay_tokens_prediction: double (nullable = true)
 |-- nt_3p_essay_tokens_prediction: double (nullable = true)
 |-- nt_100p_essay_tokens_prediction: double (nullable = true)
 |-- thoughtful_donor_essay_tokens_prediction: double (nullable = true)
 |-- is_exciting_item_tokens_prediction: double (nullable = true)
 |-- vendorid: string (nullable = true)
 |-- cost_deviation: double (nullable = true)
 |-- H1_mean_donation_distance: double (nullable = true)
 |-- H1_mean_same_teacher_donation: double (nullable = true)
 |-- H1_mean_same_school_donation: double (nullable = true)
 |-- H1_mean_neighborhood_donation: double (nullable = true)
 |-- H1_mean_outside_donation: double (nullable = true)
 |-- H2_mean_donation_distance: double (nullable = true)
 |-- H2_mean_same_teacher_donation: double (nullable = true)
 |-- H2_mean_same_school_donation: double (nullable = true)
 |-- H2_mean_neighborhood_donation: double (nullable = true)
 |-- H2_mean_outside_donation: double (nullable = true)
 |-- H3_mean_donation_distance: double (nullable = true)
 |-- H3_mean_same_teacher_donation: double (nullable = true)
 |-- H3_mean_same_school_donation: double (nullable = true)
 |-- H3_mean_neighborhood_donation: double (nullable = true)
 |-- H3_mean_outside_donation: double (nullable = true)
 |-- H1_sum_sponsored_distance: double (nullable = true)
 |-- H1_sum_same_teacher_sponsored: long (nullable = true)
 |-- H1_sum_same_school_sponsored: long (nullable = true)
 |-- H1_sum_neighborhood_sponsored: long (nullable = true)
 |-- H1_sum_outside_sponsored: long (nullable = true)
 |-- H2_sum_sponsored_distance: double (nullable = true)
 |-- H2_sum_same_teacher_sponsored: long (nullable = true)
 |-- H2_sum_same_school_sponsored: long (nullable = true)
 |-- H2_sum_neighborhood_sponsored: long (nullable = true)
 |-- H2_sum_outside_sponsored: long (nullable = true)
 |-- H3_sum_sponsored_distance: double (nullable = true)
 |-- H3_sum_same_teacher_sponsored: long (nullable = true)
 |-- H3_sum_same_school_sponsored: long (nullable = true)
 |-- H3_sum_neighborhood_sponsored: long (nullable = true)
 |-- H3_sum_outside_sponsored: long (nullable = true)
 |-- date_posted: date (nullable = true)
 |-- is_exciting: integer (nullable = true)
 |-- is_exciting_1: integer (nullable = true)
 |-- is_exciting_2: integer (nullable = true)
 |-- is_exciting_3: integer (nullable = true)
 |-- is_exciting_4: integer (nullable = true)
 |-- is_exciting_5: integer (nullable = true)
 |-- is_exciting_6: integer (nullable = true)
 |-- is_exciting_7: integer (nullable = true)
 |-- is_exciting_8: integer (nullable = true)
 |-- is_exciting_9: integer (nullable = true)
 |-- is_exciting_10: integer (nullable = true)
 |-- is_exciting_11: integer (nullable = true)
 |-- is_exciting_12: integer (nullable = true)
 |-- is_exciting_13: integer (nullable = true)
 |-- is_exciting_14: integer (nullable = true)
 |-- is_exciting_15: integer (nullable = true)
 |-- is_exciting_16: integer (nullable = true)
 |-- is_exciting_17: integer (nullable = true)
 |-- is_exciting_18: integer (nullable = true)
 |-- is_exciting_19: integer (nullable = true)
 |-- is_exciting_20: integer (nullable = true)
 |-- dealine_week: long (nullable = true)

*/