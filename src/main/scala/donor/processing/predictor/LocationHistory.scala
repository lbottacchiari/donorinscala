package donor.processing.predictor

import donor.processing.file.FileReader
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

class LocationHistory(fileReader:FileReader, aggregator:Aggregator, spark:SparkSession) {
  import spark.implicits._

  private val locationHistorizer = new Historizer(fileReader, spark)

  lazy val zipHistoricalFeatures:DataFrame = locationHistorizer.createHistoricalFeatures(evaluateLocationChunckHistory("donor_zip"), "school_zip" )
  lazy val cityHistoricalFeatures:DataFrame = locationHistorizer.createHistoricalFeatures(evaluateLocationChunckHistory("donor_city"), "school_city" )
  lazy val stateHistoricalFeatures:DataFrame = locationHistorizer.createHistoricalFeatures(evaluateLocationChunckHistory("donor_state"), "school_state" )


  private def evaluateLocationChunckHistory(locationFeature:String)(chunk:Int):DataFrame = {
    aggregator.donationByLocation
      .filter($"history_chunck" === chunk)
      .na.drop("any",Seq(locationFeature)) //drop donations whose location is unknown
      .groupBy(locationFeature)
      .agg(count(locationFeature).alias("HX_" + locationFeature  + "_nb_donations")
          ,mean($"donation_total").alias("HX_" + locationFeature  + "_mean_donation_total")
          ,sum($"donation_total").alias("HX_" + locationFeature  + "_sum_donation_total")
          ,mean($"gave_2_exciting").alias("HX_" + locationFeature  + "_mean_gave_2_exciting")
          ,sum($"gave_2_exciting").alias("HX_" + locationFeature  + "_sum_gave_2_exciting")
          )
      //we have to rename the donor location features, because we have to merge them with the project location feature later by the mean of the historizer
      .withColumnRenamed(locationFeature, locationFeature.replace("donor","school"))
      .persist()
  }

}
