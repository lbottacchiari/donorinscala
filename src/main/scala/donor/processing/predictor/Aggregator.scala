package donor.processing.predictor

import donor.processing.file.FileReader
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{Column, DataFrame, SparkSession}


/**
  * Projects have a 1 to n relationships with resources and donations.
  * The aggregator aggregates resources into cost, so that each project has 1 cost. (1 to 1 relationship)
  * Simalarly donations are aggregated into "received", how much each project receved from donors totally
  *
  * */
class Aggregator (reader:FileReader, spark:SparkSession) {
  import spark.implicits._

  //Thanks this member variable this dataframes are persisted the first time their are computed. This avoids repetitive computations in case of multiple usage
  lazy val cost: DataFrame = aggregateCosts() //compute total cost per project
  lazy val receivedPerProject: DataFrame = aggregateReceived() //compute donation received per projects
  lazy val donated: DataFrame = aggregateDonated()
  lazy val lapses: DataFrame = computeLapses()
  lazy val donationDinstance: DataFrame = evaluateDonationDistance()
  lazy val mostExpensiveItem: DataFrame = computeMostExpensiveItem()
  lazy val donationByLocation:DataFrame = evaluateDonationByLocation() //simply donations with their donor's city, state, zip and history chunck

  /*
 the data set resources has n records for each projectid. we aggregate the resources  so that each project as one cost record
  */
  private def aggregateCosts() = {
    val projectsSelect = reader.projects.select("projectid", "date_posted", "teacher_acctid", "history_chunck")
    reader.resources.groupBy("projectid").agg(sum("cost").alias("cost"))
      .join(projectsSelect, Seq("projectid")).orderBy("date_posted")
      .persist()
  }

  /*
  the data set donations has n records for each project id. we aggreate the donations so that each prohect as one received record
   */
  private def aggregateReceived() = {
    val projectsSelect = reader.projects.select("projectid", "date_posted", "teacher_acctid", "history_chunck")
    reader.donations.select("projectid","dollar_amount")
      .withColumn("d100_and_up",($"dollar_amount" === "100_and_up").cast(IntegerType)) //Attention: if dollar_amount is NA, it yields 0...
      .withColumn("d10_to_100",($"dollar_amount" === "10_to_100").cast(IntegerType))
      .withColumn("dunder_10",($"dollar_amount" === "under_10").cast(IntegerType))
      .drop("dollar_amount")
      .groupBy("projectid").agg(sum("d100_and_up").alias("d100_and_up"),sum("d10_to_100").alias("d10_to_100"),sum("dunder_10").alias("dunder_10"))
      .join(projectsSelect, Seq("projectid")).orderBy("date_posted")
      .persist()
  }

  //get info on how much teachers donated themselves and if they donated to exciting projects
  private def aggregateDonated():DataFrame = {
    val projectsSelect = reader.projects.select("projectid", "date_posted", "history_chunck")
    val outcomesSelect = reader.outcomes.select("projectid", "is_exciting")
    val filter = reader.projects.select("teacher_acctid").distinct()
    val donation = reader.donations
    donation.select("projectid","dollar_amount","donor_acctid")
      .join(filter, donation("donor_acctid") === filter("teacher_acctid"), joinType = "inner")
      .drop(donation("donor_acctid"))
      .withColumn("g100_and_up",($"dollar_amount" === "100_and_up").cast(IntegerType))
      .withColumn("g10_to_100",($"dollar_amount" === "10_to_100").cast(IntegerType))
      .withColumn("gunder_10",($"dollar_amount" === "under_10").cast(IntegerType))
      .drop("dollar_amount")
      .join(projectsSelect, Seq("projectid")) //add dates
      .join(outcomesSelect, Seq("projectid")) //add is_exciting
      .withColumnRenamed("is_exciting","gexciting")
      .drop("projectid")
      .orderBy("date_posted")
      .persist()

  }

  private def computeLapses():DataFrame = {
    def lapse(pos:Int):Column =  datediff( $"date_posted" , lag($"date_posted",pos).over(Window.partitionBy($"teacher_acctid").orderBy($"date_posted")) )  //how many days from last posted project by same teacher?

    reader.projects.select("projectid","teacher_acctid","date_posted")
      .withColumn("lapse", lapse(1))
      .withColumn("lapse2", lapse(2))
      .withColumn("lapse3", lapse(3))
      .orderBy("teacher_acctid","date_posted")

    /* TODO What for is this?
    LAPSE$lapse_m1<-pmin(c(LAPSE$lapse[-1],NA),10) #what is this? the value is taken from lapse of the next teacher's project. A value in the future....
    LAPSE$lapse_m1[is.na(LAPSE$lapse_m1)]<-10
    */
  }

  /*
      Note:
      .withColumn("max_cost_resource_id", first($"resourceid").over(Window.partitionBy($"projectid")).orderBy($"cost".desc))) This will work as expected
      .withColumn("max_cost_resource_id", last($"resourceid").over(Window.partitionBy($"projectid")).orderBy($"cost".asc))) This will NOT work as expected. I don't understand how last actualy work on an ordered partition. See https://stackoverflow.com/questions/43114445/how-to-use-first-and-last-function-in-pyspark
      .withColumn("max_cost", max($"cost").over(Window.partitionBy($"projectid")).orderBy($"cost".asc))) This will NOT work as expected. Don't use agg functions like max if orderBy is present! You don't actually need it

   */
  private def computeMostExpensiveItem():DataFrame = {
    reader.resources
      .select("resourceid", "projectid", "vendorid", "item_name", "cost")
      //.withColumn("max_cost", max($"cost").over(Window.partitionBy($"projectid"))) this does not work beacuse there could be more the one resource with the same max cost.you will end up with multiple resources for a single project
      .withColumn("cost_rank", row_number.over(Window.partitionBy($"projectid").orderBy($"cost".desc)))
      .filter($"cost_rank" === 1)
      .select("projectid", "vendorid", "item_name")
  }

  //returns the location of the school where the teacher posted his last project. This is considered to be the teacher last location
  private def evaluateTeacherLastLocation(): DataFrame = {
    //it's important that you sort before you group. Also the sort must be the same as the group + the column for which you actually want the sorting.
    reader.projects
      .select("projectid","date_posted","teacher_acctid", "school_longitude","school_latitude")
      .na.drop()
      .orderBy($"teacher_acctid",$"date_posted".desc)
      .groupBy("teacher_acctid")
      .agg(first($"school_longitude").alias("school_longitude"),first($"school_latitude").alias("school_latitude"))
  }

  //For each donation evaluate the distance between the location of the donor and the location of the school where the project was posted
  //the location of the donor can be evaluated only for the donors who posted at least one project
  private def evaluateDonationDistance(): DataFrame = {
    import functions.Location

    val donorLocation = evaluateTeacherLastLocation()
      .withColumnRenamed("school_longitude","donor_longitude")
      .withColumnRenamed("school_latitude","donor_latitude")
      .withColumnRenamed("teacher_acctid","donor_acctid")

    val projects = reader.projects.select("projectid", "history_chunck","teacher_acctid", "school_longitude","school_latitude")

    val distanceUdf = udf((donorLat:Double,donorLon:Double,schoolLat:Double,schoolLon:Double) => functions.calculateDistanceInKilometer(Location(donorLat,donorLon),Location(schoolLat,schoolLon)))

    val donationLocation = reader.donations
      .select("projectid","donor_acctid")
      .join(donorLocation,Seq("donor_acctid"),"inner")
      .join(projects,Seq("projectid"),"inner")
      .withColumn("donation_distance", distanceUdf($"donor_latitude",$"donor_longitude",$"school_latitude",$"school_longitude"))
      .withColumn("same_teacher_donation", ($"donor_acctid" === $"teacher_acctid").cast(IntegerType))  //flag donor = teacher
      .withColumn("donation_distance",when($"same_teacher_donation" ===1, 0.0).otherwise($"donation_distance")) //set distance to 0 if donor is the teacher who posted the project
       //flag if  school location is the same as the donor location,i.e. the donor is in the same school as the teacher who posted the project
      .withColumn("same_school_donation", ($"donation_distance" ===0 && $"same_teacher_donation" === 0).cast(IntegerType))
      //flag if donor is in the same neighborhood as the school where the project was posted. Within 10 miles= 16 Km radius
      .withColumn("neighborhood_donation", ($"donation_distance" > 0 && $"donation_distance" < 16.0934).cast(IntegerType))
      .withColumn("outside_donation", ($"donation_distance" >= 16.0934).cast(IntegerType))

    donationLocation.persist()
  }

  /*
 --------------------+----------------+-----------+---------+--------------+--------------+---------------+
|           projectid|      donor_city|donor_state|donor_zip|donation_total|history_chunck|gave_2_exciting|
+--------------------+----------------+-----------+---------+--------------+--------------+---------------+
|0008ac907bf237a15...|         Newhall|         CA|    91321|          55.0|          6039|              0|
   */
  private def evaluateDonationByLocation(): DataFrame = {
    val projects = reader.projects.select("projectid","history_chunck")
    val outcomes = reader.outcomes.select("projectid", "is_exciting")
    reader.donations
      .select("projectid","donor_city","donor_state", "donor_zip","donation_total")
      .join(projects,Seq("projectid"),"inner")
      .join(outcomes,Seq("projectid"),"inner")
      .withColumnRenamed("is_exciting","gave_2_exciting")
      .persist()

  }

}

/*

+--------------------------------+--------+---------------------------------------------------------+
|projectid                       |vendorid|item_name                                                |
+--------------------------------+--------+---------------------------------------------------------+
|00001ccc0e81598c4bd86bacb94d7acb|767     |DD570X - Elementary Math Instant Learning Centers - Set 1|
+--------------------------------+--------+---------------------------------------------------------+

+--------------------------------+--------+----------+
|projectid                       |vendorid|item_name |
+--------------------------------+--------+----------+
|00002d691c05c51a5fdfbb2baef0ba25|7       |Goose Girl|
|00002d691c05c51a5fdfbb2baef0ba25|7       |Redwall   |
+--------------------------------+--------+----------+

+--------------------------------+--------------------------------+--------+------------------------------------------------------------------------+------------------+------------------+
|resourceid                      |projectid                       |vendorid|item_name                                                               |cost              |max_cost          |
+--------------------------------+--------------------------------+--------+------------------------------------------------------------------------+------------------+------------------+
|5876c22cd0469fe1266024f930adb943|00001ccc0e81598c4bd86bacb94d7acb|767     |TC172GR - Mold & Play Moon Sand - Green                                 |19.99             |199.0             |
|945b27fe50fed5a16e24ff1b91bbfa24|00001ccc0e81598c4bd86bacb94d7acb|767     |BD154 - Build-A-Kid Sponge Painters                                     |12.95             |199.0             |
|4d5df6b537cb853d7a185c9b0c44d7f2|00001ccc0e81598c4bd86bacb94d7acb|767     |CG584 - Adjustable Apron - Blue                                         |7.98              |199.0             |
|04bb50881141bdd20fb332f6819d2848|00001ccc0e81598c4bd86bacb94d7acb|767     |CG639 - Roll-On Painters - 10-Color Set                                 |39.98             |199.0             |
|e9571c3a1459bc4b0a8726e744bccdaf|00001ccc0e81598c4bd86bacb94d7acb|767     |CG586 - Adjustable Apron - Yellow                                       |7.98              |199.0             |
|b36ac0fa7bc41577c5a4bdbcb53690c3|00001ccc0e81598c4bd86bacb94d7acb|767     |LK936 - Economy Paintbrush Assortment - Set of 24                       |9.99              |199.0             |
|0478ca3b1b3aba21e41b6fb7e5ef944a|00001ccc0e81598c4bd86bacb94d7acb|767     |TA301VT - Fadeless Paper Roll - Violet                                  |15.99             |199.0             |
|2ffa466fa1cbc221f90ddafee82d9663|00001ccc0e81598c4bd86bacb94d7acb|767     |RS261 - Help-Yourself Glue Tubes                                        |12.99             |199.0             |
|06d48fb6646b17eea8dd7e5fdd9a6178|00001ccc0e81598c4bd86bacb94d7acb|767     |LC987 - Newsprint Easel Paper - 16&amp;#34; x 17 1/2&#34;               |47.94             |199.0             |
|3fa71d3702bcf25d9cae190c60b299db|00001ccc0e81598c4bd86bacb94d7acb|767     |TR670X - Building Fine Motor Skills Games                               |69.5              |199.0             |
|ac02e8778210961c823afddc606c6059|00001ccc0e81598c4bd86bacb94d7acb|767     |AA887 - Face-Painting Stampers Set                                      |14.99             |199.0             |
|72f084e8c03ce9b3eff6e59de0476ef8|00001ccc0e81598c4bd86bacb94d7acb|767     |FF534 - Tissue Paper Squares                                            |9.99              |199.0             |
|0e8457adce345738b8130ca40c5b5f07|00001ccc0e81598c4bd86bacb94d7acb|767     |SD748 - Extra 1/2&#34; Tape Pack                                        |24.5              |199.0             |
|a5218fb0fdfebe1206089deabae12804|00001ccc0e81598c4bd86bacb94d7acb|767     |EV214 - Brilliant Dot Art Painters - 6-Color Set                        |29.98             |199.0             |
|dad214485bec16a1973756d15f0f4495|00001ccc0e81598c4bd86bacb94d7acb|767     |LA820X - Lakeshore No-Spill Paint Cups - 10-Color Set                   |21.98             |199.0             |
|9fa958023189824bee1f166725cfd407|00001ccc0e81598c4bd86bacb94d7acb|767     |LL459 - Geoboard Design Center                                          |24.99             |199.0             |
|95bd39e0a66c5ec21aa771f856aee86d|00001ccc0e81598c4bd86bacb94d7acb|767     |EV212 - Regular Dot Art Painters - 6-Color Set                          |29.98             |199.0             |
|2dd30e64dab940717aba51392d18a55b|00001ccc0e81598c4bd86bacb94d7acb|767     |LL208 - Add-A-Frog Math Kit                                             |34.99             |199.0             |
|d2501ba93bfb2b24f4369ee1649f8ed7|00001ccc0e81598c4bd86bacb94d7acb|767     |DD745X - Lakeshore Hands-On Math Trays - Complete Set                   |45.0              |199.0             |
|5e88489336ebc9c66614df2f14f5d3d1|00001ccc0e81598c4bd86bacb94d7acb|767     |TA301AG - Fadeless Paper Roll - Apple Green                             |15.99             |199.0             |
|f20f508ffdedb6637732316a27543231|00001ccc0e81598c4bd86bacb94d7acb|767     |TA301YB - Fadeless Paper Roll - Royal Blue                              |15.99             |199.0             |
|5ab76ccca4ae411cce6498c04e149917|00001ccc0e81598c4bd86bacb94d7acb|767     |AA888 - Washable Face Paint Assortment Pack                             |16.99             |199.0             |
|3df8a18048c9c267f0fc2681448198c7|00001ccc0e81598c4bd86bacb94d7acb|767     |LL475X - Fishing for Fun Math Games - Complete Set                      |55.0              |199.0             |
|68e5f6db0ede7148b5f4d0a49cace9d4|00001ccc0e81598c4bd86bacb94d7acb|767     |RA302 - Fraction Circles                                                |29.99             |199.0             |
|cc8c68422ea92bad569ead1f0066cf7e|00001ccc0e81598c4bd86bacb94d7acb|767     |RR388 - Classroom Clay & Dough Designer Kit                             |39.99             |199.0             |
|aac3650483a9c0a6614ebccc6c6ced90|00001ccc0e81598c4bd86bacb94d7acb|767     |LA743 - Glitter Pack - Set of 12                                        |25.98             |199.0             |
|c2ea7ecbc583410fcec74c87ff102299|00001ccc0e81598c4bd86bacb94d7acb|767     |LM941 - Kid Counters                                                    |29.99             |199.0             |
|550e2b72548e410f7109dc8fae8272d0|00001ccc0e81598c4bd86bacb94d7acb|767     |DD570X - Elementary Math Instant Learning Centers - Set 1               |199.0             |199.0             |
|4524573e6037606aeea9e77ac447dc06|00001ccc0e81598c4bd86bacb94d7acb|767     |HH906 - Days In School Activity Station                                 |39.99             |199.0             |
|0481767c406f01687721fc990a462888|00001ccc0e81598c4bd86bacb94d7acb|767     |LM927 - Super-Safe Craft Tape Center - 1/2&#34;                         |39.99             |199.0             |
|c58dc5cca16ff778fb3eb9d7f3d02efe|00001ccc0e81598c4bd86bacb94d7acb|767     |VR717 - Washable Liquid Watercolors - 8-Color Set                       |39.98             |199.0             |
|b3af2be825736f6d530314ae70e7c16c|00001ccc0e81598c4bd86bacb94d7acb|767     |CG582 - Adjustable Apron - Red                                          |7.98              |199.0             |
|c4fb483456784ccfa21072773bd8771b|00001ccc0e81598c4bd86bacb94d7acb|767     |EE498 - 3-D Geometric Shapes Tub                                        |39.98             |199.0             |
|9846db5a14a2f9ebbcab5b7925496c18|00001ccc0e81598c4bd86bacb94d7acb|767     |HH711 - Lakeshore Jumbo Glitter Painters - 12-Bottle Set                |16.99             |199.0             |
|4abba8928d619a48b1c44d10d2ae87a3|00001ccc0e81598c4bd86bacb94d7acb|767     |RR839 - Texture Brushes                                                 |22.95             |199.0             |
|911e5f96f7c5873075f518ce31341dcc|00001ccc0e81598c4bd86bacb94d7acb|767     |EE752 - Fact Family House                                               |34.99             |199.0             |
|d96183e2d575d4abdb920d137c614dde|00001ccc0e81598c4bd86bacb94d7acb|767     |PX80 - Superbright Liquid Tempera - 10-Color Set                        |39.0              |199.0             |
|532841b668aa3f58d15fcc56f974a466|00001ccc0e81598c4bd86bacb94d7acb|767     |RA132 - Foam Paint Rollers                                              |12.99             |199.0             |
|a160b9faebbb00c4eff56379845d31ca|00001ccc0e81598c4bd86bacb94d7acb|767     |BA918 - All-Purpose Paintbrush Assortment - Set of 30                   |19.99             |199.0             |
|77892d10f0b443880ab3983af889e036|00002bff514104264a6b798356fdd893|767     |TT310X - Sight-Word Learning Centers - Complete Set                     |129.0             |179.9             |
|5c02919439970f74b4fb9d3d5ec6cc37|00002bff514104264a6b798356fdd893|767     |GG136 - Magnetic Word Builders                                          |29.95             |179.9             |
|0f21bde6b606ae59bb6170759a3ae123|00002bff514104264a6b798356fdd893|767     |VC441 - Wet-Erase Markers                                               |7.99              |179.9             |
|76255d6e8f28c1482a4e41b3c89ad3f1|00002bff514104264a6b798356fdd893|767     |JJ119X - At-Your-Seat Storage Sack - Set of 10                          |179.9             |179.9             |
|213f2efdf5a97c7a9f763be918a00d8a|00002bff514104264a6b798356fdd893|767     |FF256 - Reach the Goal! Fluency Progress Chart                          |29.95             |179.9             |
|11314c17f9465b516be9f1fe197f9e1c|00002bff514104264a6b798356fdd893|767     |JJ728 - Story Wands                                                     |22.95             |179.9             |
|6b36efe575e2c246ca9d7b011dc3368d|00002d691c05c51a5fdfbb2baef0ba25|7       |Stormbreaker                                                            |83.64             |94.08             |
|8bdf461113472f5c4e4cd3cf151c210e|00002d691c05c51a5fdfbb2baef0ba25|7       |Goose Girl                                                              |94.08             |94.08             |
|e5db1583e25546a603241190ea1e499d|00002d691c05c51a5fdfbb2baef0ba25|7       |King Of The Wind                                                        |62.64             |94.08             |
|a0922b40741944793b0721488503aabc|00002d691c05c51a5fdfbb2baef0ba25|7       |Chasing Vermeer                                                         |83.64             |94.08             |
|ae86e7c1f3619cac75682c4876be1c57|00002d691c05c51a5fdfbb2baef0ba25|7       |Worth                                                                   |62.64             |94.08             |
|38680ea4e63f6cf8e62d9bd2ffb9a8a0|00002d691c05c51a5fdfbb2baef0ba25|7       |A Long Way From Chicago                                                 |73.19999999999999 |94.08             |
|484468c6d8e92f3550737f9244de06d0|00002d691c05c51a5fdfbb2baef0ba25|7       |High King                                                               |73.19999999999999 |94.08             |
|4c22f64621ff607f0fbf4aa7b24f2b67|00002d691c05c51a5fdfbb2baef0ba25|7       |Redwall                                                                 |94.08             |94.08             |
|fe1357d149b04833eeae139dd84a9c44|00002d691c05c51a5fdfbb2baef0ba25|7       |Each Little Bird That Sings                                             |62.28             |94.08             |
|e3c4c7910dc6fd6f400fd01f8ba323a0|00002d691c05c51a5fdfbb2baef0ba25|7       |Hatchet                                                                 |85.39999999999999 |94.08             |
|b4cae8ba4d89dd32b564e2a1e516b55e|0000b38bbc7252972f7984848cf58098|150     |Epson PowerLite X12   LCD projector                                     |444.51            |444.51            |
|d974eae980c30e62e4b119f1a6b9f047|0000ee613c92ddc5298bf63142996a5c|82      |Targus SafePORT - protective case for web tablet                        |37.8              |299.99            |
|0f0774d2ed6a5a936684b00463a9be12|0000ee613c92ddc5298bf63142996a5c|82      |Apple&#174; iPad&#174; mini Wi-Fi - tablet - iOS 6 - 16 GB - 7.9        |299.99            |299.99            |
|d42d8d28490deebdddf18fcb77a7fee0|0000fa3aa8f6649abab23615b546016d|7       |Sign of the Beaver SP4954                                               |9.58              |132.89999999999998|
|cb3e0041b8882a9ecd6c4023becf831c|0000fa3aa8f6649abab23615b546016d|7       |Brother Eagle, Sister Sky: A Message from Chief Seattle Susa            |13.31             |132.89999999999998|
|150085ddff27dbd1cee3c91a008976db|0000fa3aa8f6649abab23615b546016d|7       |El signo del Castor/ The Sign of the Beaver                             |9.61              |132.89999999999998|
|35aaaffce28fdf112954855e1d427319|0000fa3aa8f6649abab23615b546016d|7       |Sign of the Beaver - NU2420                                             |8.87              |132.89999999999998|
|28b04eb9b9bab440ae2728468254770c|0000fa3aa8f6649abab23615b546016d|7       |The Sign of the Beaver (Audio Book)                                     |11.09             |132.89999999999998|
|202d2fe1164630c3e18623345650175d|0000fa3aa8f6649abab23615b546016d|7       |Soft Rain                                                               |132.89999999999998|132.89999999999998|
|5599874c5b3be4efd89d28a5cccb45d8|0000fb6aea57099cc5b051acb7f52a9e|7       |The Outsiders: LitPlan Teacher Pack (Enhanced eBook) (eBook)            |21.95             |167.67            |
|7f65c82a0e2275c25b51ef06dbae6583|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Theme Activity for Ch 10-12 eLesson (eBook)            |0.99              |167.67            |
|48834d78de59f98ec30182b124b639ed|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Plot Activity for Ch 3 eLesson (eBook)                 |1.99              |167.67            |
|4da676fe390de909140c08d6fcabbfad|0000fb6aea57099cc5b051acb7f52a9e|7       |The Outsiders                                                           |167.67            |167.67            |
|1278336e14fe430e09ce02af4dd83afc|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Characterization - Ch. 1 eLesson (eBook)               |5.99              |167.67            |
|e7aa2f34724ddae46becf53e37abfb1f|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Inference Activity for Ch 7 eLesson (eBook)            |1.99              |167.67            |
|bad8b208d06eef55c88cb81b40be26bf|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Figurative Language Activity for Ch 4-5 eLesson (eBook)|0.99              |167.67            |
|106792597433d855626e5ce03923c308|0000fb6aea57099cc5b051acb7f52a9e|7       |The Outsiders: Study Guide and Student Workbook (Enhanced eBook) (eBook)|21.95             |167.67            |
|b209fe9bcca48b0f672b5e2f89897a00|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Allusions - Chapter 5 eLesson (eBook)                  |0.99              |167.67            |
|93d92bc9131b551342d116e51d3a14da|0000fb6aea57099cc5b051acb7f52a9e|7       |The Outsiders: L-I-T Guide (eBook)                                      |10.95             |167.67            |
|74417ffabe9716d41041f44de637d3b6|0000fb6aea57099cc5b051acb7f52a9e|7       |The Outsiders: Puzzle Pack (Enhanced eBook) (eBook)                     |21.95             |167.67            |
|ec7c5a57c8cfe7c169b564a32a3ac663|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Theme - Ch.9-10 eLesson (eBook)                        |0.99              |167.67            |
|fc67333b17ee10cca6a16d9aa1ff902c|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Viewpoints on Life Activity for Ch 2-3 eLesson (eBook) |1.99              |167.67            |
|fc0457c08901fa6f60b509c56fcbd48f|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders - NU3621                                                      |8.75              |167.67            |
|431146f5f7dd3ce0107c0b290c66b72e|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Novel Test (eBook)                                     |4.99              |167.67            |
|3af29cd461f603032103b76a4ae9ea1f|0000fb6aea57099cc5b051acb7f52a9e|7       |Outsiders, The - Response Journal (Enhanced eBook)                      |24.95             |167.67            |
|79d49c059c56a4a90d4be4e92ab91fa4|0000fb6aea57099cc5b051acb7f52a9e|7       |Bullying in Schools                                                     |5.0               |167.67            |
|898a36952f5a888f6106dd3bcd6b8d56|0000fb6aea57099cc5b051acb7f52a9e|7       |A Guide for Using The Outsiders in the Classroom (eBook)                |8.99              |167.67            |
|d551cd2ca61be4c0105517cdb59a1e47|0000fb6aea57099cc5b051acb7f52a9e|7       |The Outsiders Literature Guide (eBook)                                  |24.95             |167.67            |
|91a1f90a3be11f5d53d57d3a5d202f0b|0000fb6aea57099cc5b051acb7f52a9e|7       |The Outsiders Student Pack                                              |9.48              |167.67            |
|5fca7f32c69aeac5882b9e254857dc4c|0001120447a33dd9ffeefa107ed04c43|178     |STATIONARY, FROSTED TREE -- Fun Paper Letterhead; Frosted Tree          |97.08             |97.08             |
|71a91cda6283e58fdaba44e9f03a5508|0001120447a33dd9ffeefa107ed04c43|758     |The Watsons Go to Birmingham - 1963 Christopher Paul Curtis             |70.19999999999999 |97.08             |
|b6a8e5b47d56d0900ff66dfa70c5a8d0|0001120447a33dd9ffeefa107ed04c43|758     |Bud, Not Buddy Christopher Paul Curtis                                  |70.19999999999999 |97.08             |
|b595f87408da03e3752894acdedcf642|0001146d343ea9452089d0e302496c06|82      |Flip Video MinoHD 1hr - camcorder - internal flash memory               |233.96999999999997|233.96999999999997|

 */



/*
+--------------------------------+-----------+----------+---------+-----------+--------------+---------+
|teacher_acctid                  |g100_and_up|g10_to_100|gunder_10|date_posted|history_chunck|gexciting|
+--------------------------------+-----------+----------+---------+-----------+--------------+---------+
|aff385683cd7c64cd41af81e8379848d|0          |1         |0        |2003-06-19 |6011          |0        |
|b1e8765cc7300edc4cf45d3f5bd99eb1|1          |0         |0        |2003-08-06 |6011          |0        |
|ffe8bd0ee7bbe5083a6432f6aaf01e86|1          |0         |0        |2004-09-12 |6015          |0        |
|93b419c6f5fe2e1b88a64ce21f2bc6c7|0          |1         |0        |2005-04-18 |6016          |0        |
|b7a9acb7531508db28c56246f82ff3c1|0          |1         |0        |2005-09-20 |6018          |0        |
|3d307081e0ab59ab507a67ab6c02ad24|1          |0         |0        |2005-10-03 |6018          |0        |
|bcb12af551d27bc8c1ad221f537ed4b3|1          |0         |0        |2005-11-07 |6018          |0        |
|bcb12af551d27bc8c1ad221f537ed4b3|1          |0         |0        |2005-11-07 |6018          |0        |
|bcb12af551d27bc8c1ad221f537ed4b3|1          |0         |0        |2005-11-07 |6018          |0        |
|bcb12af551d27bc8c1ad221f537ed4b3|1          |0         |0        |2005-11-07 |6018          |0        |
|bcb12af551d27bc8c1ad221f537ed4b3|1          |0         |0        |2005-11-07 |6018          |0        |
|bcb12af551d27bc8c1ad221f537ed4b3|1          |0         |0        |2005-11-07 |6018          |0        |
|bcb12af551d27bc8c1ad221f537ed4b3|1          |0         |0        |2005-11-07 |6018          |0        |
|839675565075156759805d635c6a7d33|0          |1         |0        |2005-12-02 |6018          |0        |
|0d01a0a20a223e9bfb90262ad961f5b4|0          |1         |0        |2005-12-08 |6018          |0        |
|a96933389f433dafe858196ca28b104d|0          |0         |1        |2006-01-05 |6019          |0        |
|f40ffd662ea8679522f570d2c61c5797|0          |1         |0        |2006-01-17 |6019          |0        |
|f6fe75e38575b1a02f6f425e9ee8e6a4|0          |1         |0        |2006-02-28 |6019          |0        |
|f6fe75e38575b1a02f6f425e9ee8e6a4|0          |1         |0        |2006-02-28 |6019          |0        |
|2200aefa07202adf232a4fc4fc16bc1e|1          |0         |0        |2006-03-16 |6019          |0        |
|3edc20c96bb07d1a979d9b6e10b64de9|1          |0         |0        |2006-03-24 |6019          |0        |
|e0abc139fa860487396f01150efb2ccc|0          |1         |0        |2006-03-26 |6019          |0        |
|d1e0d84e92c36f2ad4b989e1b3e1106e|0          |1         |0        |2006-03-26 |6019          |0        |
|0a7682f4a9d5aafb36830a275418a089|0          |1         |0        |2006-04-12 |6019          |0        |
|a019670548ef958e8e035caaac67fd27|0          |1         |0        |2006-05-06 |6020          |0        |
|a019670548ef958e8e035caaac67fd27|0          |1         |0        |2006-05-07 |6020          |0        |
|4925e03dbbadcc8e7bff243e2e2bbf68|1          |0         |0        |2006-06-22 |6020          |0        |
|358bd00b5cab2b2a43d31ac94d53adce|0          |1         |0        |2006-08-07 |6020          |0        |
|ffea6b237f858be9a5e7e73d60689e38|1          |0         |0        |2006-08-23 |6020          |0        |
|f40ffd662ea8679522f570d2c61c5797|0          |0         |1        |2006-08-31 |6020          |0        |
|b7792d7326c50e612cce64a465361b50|0          |1         |0        |2006-09-07 |6021          |0        |
|b7792d7326c50e612cce64a465361b50|0          |1         |0        |2006-09-07 |6021          |0        |
|4fee9b1ec6784f9e135b8d0f5ab8b5ce|0          |1         |0        |2006-09-11 |6021          |0        |
|7720eb1ffaaab42a6d21d38450605e50|1          |0         |0        |2006-09-17 |6021          |0        |
|cb80010ecf7193a16e78a359120ac451|1          |0         |0        |2006-09-19 |6021          |0        |
|414b2c2391b984412e8771eeebe692e6|1          |0         |0        |2006-10-03 |6021          |0        |
|d5fb8e40c2aa3df1682894dd4201e761|1          |0         |0        |2006-10-03 |6021          |0        |
|2200aefa07202adf232a4fc4fc16bc1e|1          |0         |0        |2006-10-17 |6021          |0        |
|8d0b09f26ab9fec85113f120bf2e11ff|1          |0         |0        |2006-10-20 |6021          |0        |
|5e4ff10455145c3be2071f923aa0a8e8|0          |1         |0        |2006-10-23 |6021          |0        |
+--------------------------------+-----------+----------+---------+-----------+--------------+---------+
only showing top 40 rows

 */