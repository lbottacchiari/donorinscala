package donor.processing.predictor

/**
  * Created by lorenzobottacchiari on 05/03/17.
  */
import java.time._



object functions {

  /**
    *
    * @param date
    * @return trimester number of given date
    */
  def trimester(date:LocalDate) : Int = {
    val granularity = 4 // 12/4 = 3
    val yearSemesters = date.getYear * 12 / granularity //number of trimesters passed from year zero
    val monthSemester  = math.floor((date.getMonth.getValue - 1) / granularity) + 1 //Jan-Apr=1,May-Aug=2,Sept-Dec=3
    (yearSemesters + monthSemester).toInt // 1st Jan of year zero is in trimester 1
  }

  /**
    *
    * @param date
    * @return trimester number of given date. All dates of 2014 are put in the first trimester. test data goes from 1.1.2104 to
    *         12.5.2014. we put all test date in the first trimester of 2014for simplicity (=6043)
    */
  def splitHistory(date:LocalDate) : Int = math.min(trimester(date),trimester(LocalDate.of(2014,1,1)))

  case class Location(lat: Double, lon: Double)

  def calculateDistanceInKilometer(userLocation: Location, warehouseLocation: Location): Double = {
    val AVERAGE_RADIUS_OF_EARTH_KM = 6371
    val latDistance = Math.toRadians(userLocation.lat - warehouseLocation.lat)
    val lngDistance = Math.toRadians(userLocation.lon - warehouseLocation.lon)
    val sinLat = Math.sin(latDistance / 2)
    val sinLng = Math.sin(lngDistance / 2)
    val a = sinLat * sinLat +
      (Math.cos(Math.toRadians(userLocation.lat)) *
        Math.cos(Math.toRadians(warehouseLocation.lat)) *
        sinLng * sinLng)
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    (AVERAGE_RADIUS_OF_EARTH_KM * c)
  }



  def main(args:Array[String]):Unit = {
    var date = LocalDate.now()
    println(trimester(date));
    date = LocalDate.of(0,1,1)
    println(trimester(date));
    date = LocalDate.of(2010,12,1)
    println(trimester(date));
    date = LocalDate.of(2011,12,31)
    println(trimester(date));

    println(calculateDistanceInKilometer(Location(40.74221, -74.00204), Location(40.84169, -73.87546)))
    println(calculateDistanceInKilometer(Location(40.85868, -73.89469), Location(37.36831, -121.83874)))

  }
}
