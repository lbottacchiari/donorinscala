package donor.processing.predictor

import donor.processing.file.FileReader
import donor.processing.predictor.Historizer.projects_extract_chunks
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.collection.mutable

object Historizer {
  val history_chunck_list = 6033 to 6042
  val history_chunks_per_project = 3
  val projects_extract_chunks = (history_chunck_list.min + 3) to 6043 //(history_chunck_list.min + 4)
}

/*
Hisotorzation does folliwing.
Each project has a  teacher who posted the project
We group data by teacher and evaluate for each teacher some statistics, like how many donations he received, how many successful project he posted,...
We evaluate these statistics for each history chunk of three months

Also each project is posted in a school located in a city
We group the data by city and calculate other statistics for each city on each history chunk

Finally, we add to each project the historical features for the corresponding teacher and city,......

*/


//Some documentation on derived class TeacherHistory
class Historizer (fileReader:FileReader, spark:SparkSession ) {
  import spark.implicits._

  //Decorate each project with teacher historical features of previous 3 trimesters (H1,H2,H3)
  //Requires as parameter a function that for a given history_chunk evaluate the statistics of the teachers
  //featureWithHistory is the feature of the project data frame, for which statistics are calculated, e.g. teacher, zip, city,...
  def createHistoricalFeatures(chunkFeatureEvaluator: Int => DataFrame, featureWithHistory:String): DataFrame = {

    val projectsExtract:DataFrame = fileReader.projects
      .filter($"history_chunck" >= projects_extract_chunks.min )
      .select("projectid", "history_chunck", featureWithHistory)

    val chunkFeatureMap :mutable.Map[Int,DataFrame] = evaluateHistoryChunkMap(chunkFeatureEvaluator)

    var accumulator:DataFrame = null
    for (chunk <- projects_extract_chunks) {
      var projectsInChunk = projectsExtract.filter($"history_chunck" === chunk)
      for (i <- 1 to Historizer.history_chunks_per_project) {
        val historyData = chunkFeatureMap(chunk - i)
        var renamedHistoryData = historyData
        for (c <- historyData.columns) {
          if (c.startsWith("HX")) renamedHistoryData = renamedHistoryData.withColumnRenamed(c,c.replaceFirst("HX","H"+i))
        }
        projectsInChunk = addHistoryData(projectsInChunk, renamedHistoryData, featureWithHistory)
      }
      accumulator = if (accumulator == null) projectsInChunk else accumulator.union(projectsInChunk)
    }
    accumulator.persist()
  }


  private def addHistoryData(projects:DataFrame, historyData:DataFrame, histGroupingFeature:String) :DataFrame = {
    var projectsWithHistory = projects.join(historyData, Seq(histGroupingFeature),joinType = "left_outer")
    //left outer join produces a lot of missing values, because hitorical statistics are not avaliable for all projects
    // fill missing nb_projects by 0
    val nbCols = projectsWithHistory.columns.filter((c:String) => c.startsWith("H") && c.contains("nb"));
    projectsWithHistory = projectsWithHistory.na.fill(0,nbCols)
    //fill missing other features by -1
    val histCols = projectsWithHistory.columns.filter((c:String) => c.startsWith("H") && !c.contains("nb"));
    projectsWithHistory = projectsWithHistory.na.fill(-1,histCols)
    return projectsWithHistory
  }



  private def evaluateHistoryChunkMap(chunkFeatureEvaluator: Int => DataFrame):mutable.HashMap[Int,DataFrame] = {
    var mapTmp = mutable.HashMap[Int,DataFrame]()
    for (chunk <- Historizer.history_chunck_list ) {
      mapTmp += (chunk -> chunkFeatureEvaluator(chunk))
    }
    mapTmp
  }

}
