package donor.processing.predictor

// This are the features used by the KDD Robot R project for training (predictor). Probably this is the result of some feature selection algorithm
object FG1PredictorFeatures {

  val features = Array(
    "secondary_focus_subject", "primary_focus_subject", "school_latitude", "cost",
    "school_longitude", "Vol_teacher_acctid", "resource_type", "H1_sum_gexciting",
    "teacher_teach_for_america", "Vol_school_city", "school_zip", "H3_last_teacher_referred_count",
    "H2_last_teacher_referred_count", "school_county", "H3_mean_cost", "students_reached",
    "H1_mean_cost", "school_city", "Vol_schoolid", "Vol_school_county",
    "H2_last_d10_to_100", "school_district", "Vol_school_zip", "teacher_prefix",
    "H1_last_teacher_referred_count", "Vol_school_district", "H3_sum_teacher_referred_count", "schoolid",
    "school_metro", "H2_sum_teacher_referred_count", "school_state", "eligible_double_your_impact_match",
    "H3_last_d10_to_100", "H2_mean_cost", "Vol_school_state", "H1_last_great_messages_proportion",
    "H1_sum_teacher_referred_count", "H3_nb_outcomes_projects", "Vol_school_ncesid", "H1_sum_gunder_10",
    "H3_last_great_messages_proportion", "H1_last_non_teacher_referred_count", "H1_last_d10_to_100", "H1_last_dunder_10",
    "H1_sum_dunder_10", "school_charter", "teacher_acctid", "H1_nb_cost_projects",
    "H1_nb_donated_projects", "H2_sum_d10_to_100", "H1_sum_nt_100p", "H1_sum_t_1p_NA",
    "H3_last_non_teacher_referred_count", "H3_last_d100_and_up", "H2_last_non_teacher_referred_count", "H3_sum_great_messages_proportion",
    "H2_sum_g100_and_up", "H2_sum_t_1p_NA", "eligible_almost_home_match", "grade_level",
    "H1_nb_outcomes_projects", "poverty_level", "H1_sum_d10_to_100", "H1_sum_d100_and_up",
    "H2_last_great_messages_proportion", "H1_sum_is_exciting", "H1_sum_great_messages_proportion", "H2_nb_cost_projects",
    "H3_sum_gexciting", "H3_sum_is_exciting", "H2_sum_g10_to_100", "H2_last_d100_and_up",
    "H1_sum_thoughtful_donor", "H2_sum_great_messages_proportion", "H3_nb_donated_projects", "H2_sum_non_teacher_referred_count",
    "H1_sum_non_teacher_referred_count", "H2_nb_donated_projects", "H1_sum_fully_funded", "H1_last_is_exciting",
    "H2_sum_is_exciting", "H3_sum_dunder_10", "H2_last_dunder_10", "H3_sum_non_teacher_referred_count",
    "H2_sum_d100_and_up", "H2_nb_outcomes_projects", "school_nlns", "H3_sum_d10_to_100",
    "H2_sum_thoughtful_donor", "H1_last_d100_and_up", "H3_last_dunder_10", "H3_sum_t_1p_NA",
    "H3_sum_fully_funded", "H3_last_is_exciting", "H1_nb_received_projects", "H3_nb_cost_projects",
    "H2_sum_fully_funded", "H1_sum_g10_to_100", "H2_sum_dunder_10", "school_kipp",
    "H1_last_fully_funded", "H3_sum_nt_100p", "H3_sum_g10_to_100", "H3_sum_d100_and_up",
    "H1_sum_great_chat", "school_magnet", "school_charter_ready_promise", "H2_sum_nt_100p",
    "H3_sum_gunder_10", "teacher_ny_teaching_fellow", "H2_sum_gexciting", "H2_sum_gunder_10",
    "H2_last_is_exciting", "H3_last_fully_funded", "school_year_round",
    "lapse", "lapse2", "lapse3", "history_chunck") //"lapse_m1" removed


  val categoricalFeatures = Array("secondary_focus_subject", "primary_focus_subject", "resource_type", "teacher_teach_for_america",
    "school_zip", "school_county", "school_city", "school_district", "teacher_prefix",
    "schoolid",
    "school_metro",
    "school_state",
    "eligible_double_your_impact_match",
    "school_charter",
    "teacher_acctid",
    "eligible_almost_home_match",
    "grade_level",
    "poverty_level",
    "school_nlns",
    "school_kipp",
    "school_magnet",
    "school_charter_ready_promise",
    "teacher_ny_teaching_fellow",
    "school_year_round")


  val numericalFeatures = features.diff(categoricalFeatures)

}


/*
root
 |-- projectid: string (nullable = true)
 |-- secondary_focus_subject: string (nullable = true)
 |-- primary_focus_subject: string (nullable = true)
 |-- school_latitude: double (nullable = true)
 |-- cost: double (nullable = true)
 |-- school_longitude: double (nullable = true)
 |-- Vol_teacher_acctid: long (nullable = true)
 |-- resource_type: string (nullable = true)
 |-- H1_sum_gexciting: long (nullable = true)
 |-- teacher_teach_for_america: string (nullable = true)
 |-- Vol_school_city: long (nullable = true)
 |-- school_zip: string (nullable = true)
 |-- H3_last_teacher_referred_count: double (nullable = true)
 |-- H2_last_teacher_referred_count: double (nullable = true)
 |-- school_county: string (nullable = true)
 |-- H3_mean_cost: double (nullable = true)
 |-- students_reached: integer (nullable = true)
 |-- H1_mean_cost: double (nullable = true)
 |-- school_city: string (nullable = true)
 |-- Vol_schoolid: long (nullable = true)
 |-- Vol_school_county: long (nullable = true)
 |-- H2_last_d10_to_100: long (nullable = true)
 |-- school_district: string (nullable = true)
 |-- Vol_school_zip: long (nullable = true)
 |-- teacher_prefix: string (nullable = true)
 |-- H1_last_teacher_referred_count: double (nullable = true)
 |-- Vol_school_district: long (nullable = true)
 |-- H3_sum_teacher_referred_count: double (nullable = true)
 |-- schoolid: string (nullable = true)
 |-- school_metro: string (nullable = true)
 |-- H2_sum_teacher_referred_count: double (nullable = true)
 |-- school_state: string (nullable = true)
 |-- eligible_double_your_impact_match: string (nullable = true)
 |-- H3_last_d10_to_100: long (nullable = true)
 |-- H2_mean_cost: double (nullable = true)
 |-- Vol_school_state: long (nullable = true)
 |-- H1_last_great_messages_proportion: double (nullable = true)
 |-- H1_sum_teacher_referred_count: double (nullable = true)
 |-- H3_nb_outcomes_projects: long (nullable = true)
 |-- Vol_school_ncesid: long (nullable = true)
 |-- H1_sum_gunder_10: long (nullable = true)
 |-- H3_last_great_messages_proportion: double (nullable = true)
 |-- H1_last_non_teacher_referred_count: double (nullable = true)
 |-- H1_last_d10_to_100: long (nullable = true)
 |-- H1_last_dunder_10: long (nullable = true)
 |-- H1_sum_dunder_10: long (nullable = true)
 |-- school_charter: string (nullable = true)
 |-- teacher_acctid: string (nullable = true)
 |-- H1_nb_cost_projects: long (nullable = true)
 |-- H1_nb_donated_projects: long (nullable = true)
 |-- H2_sum_d10_to_100: long (nullable = true)
 |-- H1_sum_nt_100p: long (nullable = true)
 |-- H1_sum_t_1p_NA: long (nullable = true)
 |-- H3_last_non_teacher_referred_count: double (nullable = true)
 |-- H3_last_d100_and_up: long (nullable = true)
 |-- H2_last_non_teacher_referred_count: double (nullable = true)
 |-- H3_sum_great_messages_proportion: double (nullable = true)
 |-- H2_sum_g100_and_up: long (nullable = true)
 |-- H2_sum_t_1p_NA: long (nullable = true)
 |-- eligible_almost_home_match: string (nullable = true)
 |-- grade_level: string (nullable = true)
 |-- H1_nb_outcomes_projects: long (nullable = true)
 |-- poverty_level: string (nullable = true)
 |-- H1_sum_d10_to_100: long (nullable = true)
 |-- H1_sum_d100_and_up: long (nullable = true)
 |-- H2_last_great_messages_proportion: double (nullable = true)
 |-- H1_sum_is_exciting: long (nullable = true)
 |-- H1_sum_great_messages_proportion: double (nullable = true)
 |-- H2_nb_cost_projects: long (nullable = true)
 |-- H3_sum_gexciting: long (nullable = true)
 |-- H3_sum_is_exciting: long (nullable = true)
 |-- H2_sum_g10_to_100: long (nullable = true)
 |-- H2_last_d100_and_up: long (nullable = true)
 |-- H1_sum_thoughtful_donor: long (nullable = true)
 |-- H2_sum_great_messages_proportion: double (nullable = true)
 |-- H3_nb_donated_projects: long (nullable = true)
 |-- H2_sum_non_teacher_referred_count: double (nullable = true)
 |-- H1_sum_non_teacher_referred_count: double (nullable = true)
 |-- H2_nb_donated_projects: long (nullable = true)
 |-- H1_sum_fully_funded: long (nullable = true)
 |-- H1_last_is_exciting: integer (nullable = true)
 |-- H2_sum_is_exciting: long (nullable = true)
 |-- H3_sum_dunder_10: long (nullable = true)
 |-- H2_last_dunder_10: long (nullable = true)
 |-- H3_sum_non_teacher_referred_count: double (nullable = true)
 |-- H2_sum_d100_and_up: long (nullable = true)
 |-- H2_nb_outcomes_projects: long (nullable = true)
 |-- school_nlns: string (nullable = true)
 |-- H3_sum_d10_to_100: long (nullable = true)
 |-- H2_sum_thoughtful_donor: long (nullable = true)
 |-- H1_last_d100_and_up: long (nullable = true)
 |-- H3_last_dunder_10: long (nullable = true)
 |-- H3_sum_t_1p_NA: long (nullable = true)
 |-- H3_sum_fully_funded: long (nullable = true)
 |-- H3_last_is_exciting: integer (nullable = true)
 |-- H1_nb_received_projects: long (nullable = true)
 |-- H3_nb_cost_projects: long (nullable = true)
 |-- H2_sum_fully_funded: long (nullable = true)
 |-- H1_sum_g10_to_100: long (nullable = true)
 |-- H2_sum_dunder_10: long (nullable = true)
 |-- school_kipp: string (nullable = true)
 |-- H1_last_fully_funded: integer (nullable = true)
 |-- H3_sum_nt_100p: long (nullable = true)
 |-- H3_sum_g10_to_100: long (nullable = true)
 |-- H3_sum_d100_and_up: long (nullable = true)
 |-- H1_sum_great_chat: long (nullable = true)
 |-- school_magnet: string (nullable = true)
 |-- school_charter_ready_promise: string (nullable = true)
 |-- H2_sum_nt_100p: long (nullable = true)
 |-- H3_sum_gunder_10: long (nullable = true)
 |-- teacher_ny_teaching_fellow: string (nullable = true)
 |-- H2_sum_gexciting: long (nullable = true)
 |-- H2_sum_gunder_10: long (nullable = true)
 |-- H2_last_is_exciting: integer (nullable = true)
 |-- H3_last_fully_funded: integer (nullable = true)
 |-- school_year_round: string (nullable = true)
 |-- lapse: integer (nullable = true)
 |-- lapse2: integer (nullable = true)
 |-- lapse3: integer (nullable = true)
 |-- history_chunck: integer (nullable = true)
 |-- is_exciting: integer (nullable = true)
 |-- dealine_days: double (nullable = true)
 */