package donor.processing.response

import donor.processing.file.FileReader
import donor.processing.predictor.functions
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types.{DateType, IntegerType}
import org.apache.spark.sql.functions._

/**
  * Created by LBottacchiari_ext on 11.04.2017.
  */
class FG1ResponseProcessor(fileReader: FileReader, spark:SparkSession) {
  import spark.implicits._

  /*
  All projects are provided with the censored outcome is_exiting. For project after 01.01.2014, is_exciting will be null but we add feature dealind_days = distance in days to last posted project of all(12.05.2014)
   */
  def createResponse():DataFrame = {
    val projects = fileReader.projects.select("projectid","date_posted")
    val triplicatedCensoredOutcome = createTriplicatedCensoredOutcome().select($"projectid",$"is_exciting_censored".alias("is_exciting"),$"dealine_days")

    //For test data dealine_days are the days remaining to  cut off date
    projects
      .join(triplicatedCensoredOutcome,Seq("projectid"),joinType = "left")
      .withColumn("max_date", max($"date_posted").over())
      .withColumn("dealine_days", when(isnull($"dealine_days"),datediff($"max_date",$"date_posted")).otherwise($"dealine_days"))
      .drop("max_date")
      .drop("date_posted")

  }

  /**
    * #censoring: the projects, whose is_exciting must be predicted, have been posted beween 1.1.2014 and 12.5.2014. Interval of 131 days
      #12.5.2014 is the cut off dated. these project are classificated as exciting if they fullfilled the requirements before 12.5.2015. In other
      # words, to achive the exiting classification,  they had as many days at their disposal as the interval between their posting date and
      # the 12.5.2014. This is a number of days between 0 and 131. In reality they might have become exiting sometime after ther 12.5.2014.
      #To simulate this in the training data, we assign a random number of days between 0 and 131 to all projects of the training set. If they took
      # more days to become exiting than this random number, we set them as non-exiting.
    */
  def createTriplicatedCensoredOutcome():DataFrame = {

    def randomIntFunc(seed:Int) = {
      val r = new scala.util.Random(seed)
      udf(() => r.nextInt(131))
    }

    val outcomesWithDate = createOutcomeWithExcitingDate()
    outcomesWithDate
      .union(outcomesWithDate)
      .union(outcomesWithDate)
      .withColumn("dealine_days", round(rand(2392983)*131)) //random integer between 0 and 131 with seed 2392983,  random_number = Math.round(Math.random()*(upper_bound - lower_bound) + lower_bound);
      .withColumn("is_exciting_censored", when($"is_exciting_days" >  $"dealine_days",0).otherwise($"is_exciting") )

  }

  def createOutcomeWithExcitingDate():DataFrame = {
    val projects = fileReader.projects.select("projectid","date_posted")
    val outcomes = fileReader.outcomes.select("projectid","is_exciting")
    val maxDonationDate = fileReader.donations.select("projectid","donation_timestamp").groupBy("projectid").agg(max("donation_timestamp").alias("max_donation_date"))

    //For an exciting project, we consider the max donation date as the date when the project became exiting
    val outcomesWithDate = outcomes
      .join(maxDonationDate,Seq("projectid"),joinType = "left")
      .withColumnRenamed("max_donation_date","is_exciting_date")
      .withColumn("is_exciting_date",when($"is_exciting" === 0,null).otherwise($"is_exciting_date").cast(DateType)) //add column is_exciting_date
      .join(projects,Seq("projectid")) //add column "date_posted"
      .withColumn("is_exciting_days", datediff($"is_exciting_date",$"date_posted")) //how many days the project took to become exciting since its posting

    outcomesWithDate
  }


/*
Response
+--------------------+-----------+-----------+------------+
|           projectid|date_posted|is_exciting|dealine_days|
+--------------------+-----------+-----------+------------+
|0ae8b5b24c2208cfd...| 2009-05-07|          0|       113.0|
|0ae8b5b24c2208cfd...| 2009-05-07|          0|        79.0|
|0ae8b5b24c2208cfd...| 2009-05-07|          0|         1.0|
|0b9953260e8e82e8c...| 2013-02-20|          0|         5.0|
|0b9953260e8e82e8c...| 2013-02-20|          0|        45.0|
|0b9953260e8e82e8c...| 2013-02-20|          0|        32.0|
|115ba7467668da534...| 2010-12-09|          0|       114.0|
|115ba7467668da534...| 2010-12-09|          0|        48.0|
|115ba7467668da534...| 2010-12-09|          0|        83.0|
|29d16b4931cc3b165...| 2010-07-27|          0|        63.0|
|29d16b4931cc3b165...| 2010-07-27|          0|        10.0|
|29d16b4931cc3b165...| 2010-07-27|          0|        32.0|
|2a190c2e2f90ea64a...| 2011-02-02|          0|        67.0|
|2a190c2e2f90ea64a...| 2011-02-02|          0|        98.0|
|2a190c2e2f90ea64a...| 2011-02-02|          0|         4.0|
|30e67ed4f66540121...| 2009-09-08|          0|        40.0|
|30e67ed4f66540121...| 2009-09-08|          0|        58.0|
|30e67ed4f66540121...| 2009-09-08|          0|        83.0|
|34dd3e2f52ee18af4...| 2003-02-07|          0|        34.0|
|34dd3e2f52ee18af4...| 2003-02-07|          0|        75.0|
|34dd3e2f52ee18af4...| 2003-02-07|          0|       123.0|
|3e685cfea3be935c4...| 2013-05-07|          0|       107.0|
|3e685cfea3be935c4...| 2013-05-07|          0|       129.0|
|3e685cfea3be935c4...| 2013-05-07|          0|        94.0|
|4327c4b35cfccd7d0...| 2010-09-23|          0|       118.0|
|4327c4b35cfccd7d0...| 2010-09-23|          0|        46.0|
|4327c4b35cfccd7d0...| 2010-09-23|          0|        51.0|
|458e8247f448ff046...| 2009-10-29|          0|        74.0|
|458e8247f448ff046...| 2009-10-29|          0|         6.0|
|458e8247f448ff046...| 2009-10-29|          0|        83.0|
|5fb9a137285daa261...| 2013-10-11|          0|       127.0|
|5fb9a137285daa261...| 2013-10-11|          0|        43.0|
|5fb9a137285daa261...| 2013-10-11|          0|        17.0|
|6840bac3de08948fa...| 2010-08-02|          0|        10.0|
|6840bac3de08948fa...| 2010-08-02|          0|       124.0|
|6840bac3de08948fa...| 2010-08-02|          0|        96.0|
|777bcfa7a523aa826...| 2012-04-09|          1|        21.0|
|777bcfa7a523aa826...| 2012-04-09|          1|        20.0|
|777bcfa7a523aa826...| 2012-04-09|          1|       106.0|
|7a9ba6745dee609ad...| 2008-12-14|          0|        89.0|
|7a9ba6745dee609ad...| 2008-12-14|          0|        16.0|
|7a9ba6745dee609ad...| 2008-12-14|          0|       101.0|
|7ece6b2980a23c2d5...| 2012-12-16|          0|        12.0|
|7ece6b2980a23c2d5...| 2012-12-16|          0|        90.0|
|7ece6b2980a23c2d5...| 2012-12-16|          0|        59.0|
|8c5063b121eb5d8ba...| 2012-11-20|          0|         0.0|
|8c5063b121eb5d8ba...| 2012-11-20|          0|        46.0|
|8c5063b121eb5d8ba...| 2012-11-20|          0|        36.0|
|99af606be398f482b...| 2006-09-09|          0|         3.0|
|99af606be398f482b...| 2006-09-09|          0|       112.0|
|99af606be398f482b...| 2006-09-09|          0|        19.0|
|ab96f452100bdcc88...| 2013-09-14|          0|        94.0|
|ab96f452100bdcc88...| 2013-09-14|          0|        98.0|
|ab96f452100bdcc88...| 2013-09-14|          0|        50.0|
|b82127d11f9eb8716...| 2014-04-14|       null|        27.0|
|ba34ff7a871dca882...| 2012-08-25|          0|         5.0|
|ba34ff7a871dca882...| 2012-08-25|          0|        94.0|
|ba34ff7a871dca882...| 2012-08-25|          0|        14.0|
|bbd356e232dfbca0b...| 2011-11-04|          0|        92.0|
|bbd356e232dfbca0b...| 2011-11-04|          0|       124.0|

 */



}
