package donor.processing.response

import donor.processing.file.FileReader
import org.apache.spark.sql.functions.{datediff, isnull, max, when, floor}
import org.apache.spark.sql.types.{DateType, IntegerType}
import org.apache.spark.sql.{DataFrame, SparkSession}

//20 week outcome censoring
class FG2ResponseProcessor (fileReader: FileReader, spark:SparkSession) {
  import spark.implicits._

  def createResponse():DataFrame = {
    val projects = fileReader.projects.select("projectid","date_posted")
    val outcomeWithExcitingWeek = evaluateOutcomeWithExcitingWeek()

    //group test data by weeks
    projects
      .join(outcomeWithExcitingWeek,Seq("projectid"),joinType = "left")
      .withColumn("max_date", max($"date_posted").over())
      .withColumn("dealine_days", when(isnull($"is_exciting"),datediff($"max_date",$"date_posted")))
      .withColumn("dealine_week", floor($"dealine_days"/7 + 1))
      .drop("max_date")
      .drop("dealine_days")

  }

  //For an exciting project, we consider the max donation date as the date when the project became exiting
  def evaluateOutcomeWithExcitingDate() : DataFrame = {
    val projects = fileReader.projects.select("projectid","date_posted")
    val outcomes = fileReader.outcomes.select("projectid","is_exciting")
    val maxDonationDate = fileReader.donations.select("projectid","donation_timestamp").groupBy("projectid").agg(max("donation_timestamp").alias("max_donation_date"))

    outcomes
      .join(maxDonationDate,Seq("projectid"),joinType = "left")
      .withColumnRenamed("max_donation_date","is_exciting_date")
      .withColumn("is_exciting_date",when($"is_exciting" === 0,null).otherwise($"is_exciting_date").cast(DateType)) //add column is_exciting_date
      .join(projects,Seq("projectid")) //add column "date_posted"
      .withColumn("is_exciting_days", datediff($"is_exciting_date",$"date_posted")) //how many days the project took to become exciting since its posting

  }


  def evaluateOutcomeWithExcitingWeek(): DataFrame = {
    val weeks = 1 to 20
    var outcomeWithExcitingWeek = evaluateOutcomeWithExcitingDate()
    for (w <- weeks) {
      outcomeWithExcitingWeek = outcomeWithExcitingWeek.withColumn("is_exciting_"+w, when($"is_exciting" === 0,false).otherwise($"is_exciting_days" <= 7*w -1).cast(IntegerType))
    }
    outcomeWithExcitingWeek
      .drop("date_posted")
      .drop("is_exciting_date")
      .drop("is_exciting_days")
  }

}
