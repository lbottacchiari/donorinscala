package donor.processing.file

import java.io.FileWriter

import scala.io.Source

private class CharSeqSieve(seq:String) {
  private val sieve = Array.fill[Option[Char]](seq.length)(None)

  override def toString():String = {
    val builder = new StringBuilder()
    for (Some(s)<-sieve) builder += s
    builder.toString
  }

  def add(c:Char):Option[Char] = {
    val freeSlot = sieve.indexOf(None)
    if (freeSlot != -1) {
      sieve(freeSlot) = Some(c)
      checkSequence()
      return None
    } else { //return the first char and shift the others
      val extractedChar = sieve(0)
      for (i<- 0 until sieve.length-1) {
        sieve(i) = sieve(i+1)
      }
      sieve(sieve.length-1) = Some(c)
      checkSequence()
      extractedChar
    }
  }

  private def checkSequence() = {
    if (toString == seq) {
      for (i <- 0 until sieve.length) sieve(i) = None
    }
  }
}


object CharSequenceRemover {
  def processFile(inputFile:String, outputFile:String, seq:String):Unit = {
    val source = Source.fromFile(inputFile, "UTF-8")
    val sieve = new CharSeqSieve(seq)
    val out = new FileWriter(outputFile)
    for (c <- source) {
      sieve.add(c) match {
        case Some(sievedChar) => out.append(sievedChar)
        case None =>
      }
    }
    out.append(sieve.toString)
    out.close()
  }

  def processString(input:String, seq:String) :String  = {
    val sieve = new CharSeqSieve(seq)
    val builder = new StringBuilder()
    for (c <- input) {
      sieve.add(c) match {
        case Some(sievedChar) => builder += sievedChar
        case None =>
      }
    }
    builder append sieve.toString
    builder.toString
  }


  def main(args: Array[String]) = {
    /*  println(processString("Lorenzo\u000d\u000a Bottacchiari","\u000d\u000a")) */
    /* processFile("/Users/lorenzobottacchiari/Development/projects/donorinscala/data/input/resources.csv","/Users/lorenzobottacchiari/Development/projects/donorinscala/data/input/cleaned_resources.csv","\u000d\u000a") */
    processFile("data/input/resources.csv","data/input/resources2.csv","\u000d\u000a") //CR+LF
    processFile("data/input/resources2.csv","data/input/resources3.csv","\u2028") //Line Separator from javascript

  }
}
