package donor.processing.file


import donor.Config
import donor.processing.predictor.functions
import org.apache.hadoop.conf.Configuration
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StructField, _}
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.collection.mutable.ListBuffer
/**
  * Created by lorenzobottacchiari on 26/02/17.
  */
class FileReader(spark: SparkSession, sample:Boolean = false) {

  // For implicit conversions like converting RDDs to DataFrames
  import spark.implicits._

  lazy val projects: DataFrame = readProjects()
  lazy val resources: DataFrame = readResources()
  lazy val donations: DataFrame = readDonations()
  lazy  val outcomes: DataFrame = readOutcomes()
  lazy val essays: DataFrame = readEssays()

  private def readProjects(): DataFrame = {

    if (sample) return spark.read.load(Config.inputDir + "projects_sample")

    val fileName = "projects.csv"
    val projectsFileName = Config.inputDir + fileName

    //nullable seems to have no effect
    val projectFields = List(
      StructField("projectid", StringType, false),
      StructField("teacher_acctid", StringType, false),
      StructField("schoolid", StringType, false),
      StructField("school_ncesid", StringType, true),
      StructField("school_latitude", DoubleType, true),
      StructField("school_longitude", DoubleType, true),
      StructField("school_city", StringType, true),
      StructField("school_state", StringType, true),
      StructField("school_zip", StringType, true),
      StructField("school_metro", StringType, true),
      StructField("school_district", StringType, true),
      StructField("school_county", StringType, true),
      StructField("school_charter", StringType, true),
      StructField("school_magnet", StringType, true),
      StructField("school_year_round", StringType, true),
      StructField("school_nlns", StringType, true),
      StructField("school_kipp", StringType, true),
      StructField("school_charter_ready_promise", StringType, true),
      StructField("teacher_prefix", StringType, true),
      StructField("teacher_teach_for_america", StringType, true),
      StructField("teacher_ny_teaching_fellow", StringType, true),
      StructField("primary_focus_subject", StringType, true),
      StructField("primary_focus_area", StringType, true),
      StructField("secondary_focus_subject", StringType, true),
      StructField("secondary_focus_area", StringType, true),
      StructField("resource_type", StringType, true),
      StructField("poverty_level", StringType, true),
      StructField("grade_level", StringType, true),
      StructField("fulfillment_labor_materials", DoubleType, true),
      StructField("total_price_excluding_optional_support", DoubleType, true),
      StructField("total_price_including_optional_support", DoubleType, true),
      StructField("students_reached", IntegerType, true),
      StructField("eligible_double_your_impact_match", StringType, true),
      StructField("eligible_almost_home_match", StringType, true),
      StructField("date_posted", DateType, true)
    )

    val splitHistoryUDF = udf((date: java.sql.Date) => functions.splitHistory(date.toLocalDate))

    spark.read
      .option("header", true)
      .option("mode", "FAILFAST")
      .schema(StructType(projectFields))
      .csv(projectsFileName)
      .withColumn("history_chunck", splitHistoryUDF($"date_posted"))
      .persist()
    //.withColumn("school_charter",$"school_charter" === "f")
 }

  private def readResources(): DataFrame = {
    if (sample) return spark.read.load(Config.inputDir + "resources_sample")

    val fileName = "resources4.csv"
    val resourceFileName = Config.inputDir + fileName

    val resourceFields = List(
      StructField("resourceid", StringType),
      StructField("projectid", StringType),
      StructField("vendorid", StringType),
      StructField("vendor_name", StringType),
      StructField("project_resource_type", StringType),
      StructField("item_name", StringType),
      StructField("item_number", StringType),
      StructField("item_unit_price", DoubleType),
      StructField("item_quantity", DoubleType)
    )

    spark.read
      .option("header", true)
      .option("mode", "FAILFAST")
      /*  .option("mode","DROPMALFORMED")*/
      .option("escape", "\"")
      .schema(StructType(resourceFields))
      .csv(resourceFileName)
      .withColumn("cost", $"item_unit_price" * $"item_quantity")
      .persist()

  }



  private def readDonations():DataFrame = {
    if (sample) return spark.read.load(Config.inputDir + "donations_sample")

    val fileName = "donations3.csv"
    val donationsFileName = Config.inputDir + fileName

    val donationFields = List(
      StructField("donationid", StringType),
      StructField("projectid", StringType),
      StructField("donor_acctid", StringType),
      StructField("donor_city", StringType),
      StructField("donor_state", StringType),
      StructField("donor_zip", StringType),
      StructField("is_teacher_acct", StringType),
      StructField("donation_timestamp", TimestampType),
      StructField("donation_to_project", DoubleType),
      StructField("donation_optional_support", DoubleType),
      StructField("donation_total", DoubleType),
      StructField("dollar_amount", StringType),
      StructField("donation_included_optional_support", StringType),
      StructField("payment_method", StringType),
      StructField("payment_included_acct_credit", StringType),
      StructField("payment_included_campaign_gift_card", StringType),
      StructField("payment_included_web_purchased_gift_card", StringType),
      StructField("payment_was_promo_matched", StringType),
      StructField("via_giving_page", StringType),
      StructField("for_honoree", StringType),
      StructField("donation_message", StringType)
    )

    val stringLength = udf((s: String) => Option(s) match {
      case Some(s) => s.trim.length
      case None => 0
    })


    spark.read
      .option("header", true)
      .option("mode", "FAILFAST")
      .option("escape", "\"")
      .schema(StructType(donationFields))
      .csv(donationsFileName)
      .withColumn("message_length", stringLength($"donation_message"))
      .persist()
  }

  private def readOutcomes(): DataFrame = {
    if (sample) return spark.read.load(Config.inputDir + "outcomes_sample")

    val fileName = "outcomes.csv"
    val outcomesFileName = Config.inputDir + fileName


    val boolFields = List("is_exciting","t_1p", "fully_funded", "green", "great_chat", "nt_3p", "nt_100p", "thoughtful_donor")
    val doubleFields = List("great_messages_proportion", "teacher_referred_count", "non_teacher_referred_count")

    val outcomeFields = ListBuffer(
      StructField("projectid", StringType)
    )

    boolFields.foreach(outcomeFields +=  StructField(_, StringType))
    doubleFields.foreach(outcomeFields += StructField(_, DoubleType))

    var allOutcomes = spark.read
      .option("header", true)
      .option("mode", "FAILFAST")
      .option("escape", "\"")
      .schema(StructType(outcomeFields))
      .csv(outcomesFileName)

    //recode variables: for each feature, one column that says if observation is Null (_NA), one column with the actual value (0 if it was Null)
    //note: is_exciting is never null in the outcome dataset
    (boolFields ::: doubleFields).foreach(s => allOutcomes = allOutcomes.withColumn(s + "_NA", allOutcomes(s).isNull.cast(IntegerType)))
    boolFields.foreach(s => allOutcomes = allOutcomes.withColumn(s, (allOutcomes(s) === "t").cast(IntegerType)))
    allOutcomes = allOutcomes.na.fill(0, boolFields ::: doubleFields)

    val projectsSelect = projects.select("projectid", "date_posted", "teacher_acctid", "history_chunck")
    allOutcomes = allOutcomes.join(projectsSelect, Seq("projectid")).orderBy("date_posted")

    allOutcomes.persist()
  }

  private def readEssays():DataFrame = {
    if (sample)
      return spark.read.load(Config.inputDir + "essays_sample")

    val filename = "essays3.csv"
    val essaysFileName = Config.inputDir + filename

    val essayFields = Array[StructField] (
      StructField("projectid", StringType),
      StructField("teacher_acctid", StringType),
      StructField("title", StringType),
      StructField("short_description", StringType),
      StructField("need_statement", StringType),
      StructField("essay", StringType)
    )

    spark.read
      .option("header", true)
      .option("mode", "FAILFAST")
      .option("escape", "\"")
      .schema(StructType(essayFields))
      .csv(essaysFileName)
      //.persist()
  }

  def saveSamples() = {
    var projectsSample = projects.sample(withReplacement = false, 0.01, 1234L).persist()
    /*
    var resourcesSample = resources.join(projectsSample.select("projectid"), Seq("projectid"))
    var donationsSample = donations.join(projectsSample.select("projectid"), Seq("projectid"))
    var outcomesSample = outcomes.join(projectsSample.select("projectid"), Seq("projectid"))
    */
    var essaysSample = essays.join(projectsSample.select("projectid"), Seq("projectid"))
    /*
    projectsSample.write.mode("overwrite").save(Config.inputDir +"projects_sample")
    resourcesSample.write.mode("overwrite").save(Config.inputDir +"resources_sample")
    donationsSample.write.mode("overwrite").save(Config.inputDir +"donations_sample")
    outcomesSample.write.mode("overwrite").save(Config.inputDir +"outcomes_sample")
    */
    essaysSample.write.mode("overwrite").save(Config.inputDir +"essays_sample")
    //essaysSample.write.option("header", true).mode("overwrite").csv(Config.inputDir +"essays_sample_R.csv")
  }

  //not used because it doesn't work properly, but here to see how to use newAPIHadoopFile
  def cleanResources() = {
    val resourcesFileName = Config.inputDir + "resources.csv"
    val cleanedResourceFileName = Config.inputDir + "cleaned_resources.csv"

    val hadoopConf: Configuration = new Configuration()
    hadoopConf.set("textinputformat.record.delimiter", "\u000A")

    //rdd of (LongWritable,Text) you have to transform in RDD of Strings before using it, otherwise not Serializable exception will rise
    //remove unwanted characters: carriage returns CR (decimal char 13, ex 000d) and character sequence '\n'
    val resourceRDD = spark.sparkContext.newAPIHadoopFile(resourcesFileName, classOf[org.apache.hadoop.mapreduce.lib.input.TextInputFormat], classOf[org.apache.hadoop.io.LongWritable], classOf[org.apache.hadoop.io.Text], hadoopConf)
      .map(l => l._2.toString).map(l => l.replaceAll("\u000D", "")).map(l => l.replaceAll("\n", ""))

    /*
    //use custom defined PatternInputFormat
    hadoopConf.set("record.delimiter.regex", myregex)
    val resourceRDD = spark.sparkContext.newAPIHadoopFile(resourcesFileName, classOf[org.apache.hadoop.mapreduce.lib.input.PatternInputFormat], classOf[org.apache.hadoop.io.LongWritable], classOf[org.apache.hadoop.io.Text], hadoopConf)
    */

    println(resourceRDD.count)
    resourceRDD.coalesce(1).saveAsTextFile(cleanedResourceFileName)
  }

}
