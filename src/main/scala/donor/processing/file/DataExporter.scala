package donor.processing.file

import donor.Config
import donor.processing.predictor.{Aggregator, FG1PredictorFeatures}
import org.apache.spark.ml.feature.{CountVectorizer, IDF, NGram}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

/**
  * Created by LBottacchiari_ext on 24.08.2017.
  */
class DataExporter(reader: FileReader, aggregator: Aggregator, spark: SparkSession) {

  import spark.implicits._
  import org.apache.spark.sql.expressions.Window

  /**
    * Extract essays data and most expensive item from project resouces, so that they can be processed in python project DonorNlp with library NLTK
    */
  def exportNlpDataSet() = {
    //TODO we condider only data from 2013, as in KDD datarobot R project
    val projects = reader.projects.filter($"history_chunck" >= 6040).select("projectid", "history_chunck", "primary_focus_area")
    val essays = reader.essays.select("projectid", "title", "essay")
    val outcomes = reader.outcomes.select("projectid", "is_exciting", "great_messages_proportion", "teacher_referred_count", "non_teacher_referred_count", "t_1p", "green", "great_chat", "nt_3p", "nt_100p", "thoughtful_donor")
    //outcomes.printSchema()
    val items = aggregator.mostExpensiveItem.select("projectid", "item_name")
    val nlpdata = projects.join(essays, Seq("projectid"), "inner").join(items, Seq("projectid"), "left").join(outcomes, Seq("projectid"), "left")
    nlpdata.write.mode("overwrite").save(Config.outputDir + "NLP/nlpdata")
    nlpdata.show(truncate = false)
  }


  def exportProjectDataForCostStackedPrediction() = {
    val projects = reader.projects
      .filter($"history_chunck" >= 6040)
      .select("projectid", "primary_focus_subject", "grade_level","students_reached")

    val costs = aggregator.cost.select("projectid", "cost")

    val windowSpec = Window.partitionBy("primary_focus_subject", "grade_level")
    //Fill null cost values with the average on the same group
    val export = projects.join(costs,Seq("projectid"),"left_outer")
      .na.fill("UNKNOWN",Seq("primary_focus_subject", "grade_level"))
      .withColumn("cost", when($"cost".isNotNull, $"cost").otherwise(mean($"cost").over(windowSpec)))
      .withColumn("students_reached", when($"students_reached".isNotNull, $"students_reached").otherwise(mean($"students_reached").over(windowSpec)))
      .withColumn("cost", when($"cost".isNotNull, $"cost").otherwise(0))
      .na.fill(0,Seq("cost","students_reached")) //needed when mean values are null for a group because all elements are nulls in the group

    export.write.mode("overwrite").save(Config.outputDir + "costdata")
    export.show(numRows = 100)
    //println(export.filter($"cost".isNull ).count())
  }

}


/*
outcomes
 |-- projectid: string (nullable = true)
 |-- is_exciting: integer (nullable = false)
 |-- t_1p: integer (nullable = false)
 |-- fully_funded: integer (nullable = false)
 |-- green: integer (nullable = false)
 |-- great_chat: integer (nullable = false)
 |-- nt_3p: integer (nullable = false)
 |-- nt_100p: integer (nullable = false)
 |-- thoughtful_donor: integer (nullable = false)
 |-- great_messages_proportion: double (nullable = false)
 |-- teacher_referred_count: double (nullable = false)
 |-- non_teacher_referred_count: double (nullable = false)
 |-- is_exciting_NA: integer (nullable = false)
 |-- t_1p_NA: integer (nullable = false)
 |-- fully_funded_NA: integer (nullable = false)
 |-- green_NA: integer (nullable = false)
 |-- great_chat_NA: integer (nullable = false)
 |-- nt_3p_NA: integer (nullable = false)
 |-- nt_100p_NA: integer (nullable = false)
 |-- thoughtful_donor_NA: integer (nullable = false)
 |-- great_messages_proportion_NA: integer (nullable = false)
 |-- teacher_referred_count_NA: integer (nullable = false)
 |-- non_teacher_referred_count_NA: integer (nullable = false)
 |-- date_posted: date (nullable = true)
 |-- teacher_acctid: string (nullable = true)
 |-- history_chunck: integer (nullable = true)

 Response_names=c("is_exciting_num",
                 "great_messages_proportion","teacher_referred_count",
                 "non_teacher_referred_count",
                 "t_1p_num","green_num","great_chat_num",
                 "nt_3p_num","nt_100p_num","thoughtful_donor_num")



+--------------------+---------------------+-------------+----------------+----+------------------+------------------+
|           projectid|primary_focus_subject|  grade_level|students_reached|cost|         mean_cost|             cost2|
+--------------------+---------------------+-------------+----------------+----+------------------+------------------+
|0cc5e22a38438b372...|          Visual Arts|  Grades 9-12|              50|null| 625.2050516647524| 625.2050516647524|
|ec947ca258c7f3f0c...|          Visual Arts|  Grades 9-12|              50|null| 625.2050516647524| 625.2050516647524|
|9ffe938023f90fcf0...|          Visual Arts|  Grades 9-12|              20|null| 625.2050516647524| 625.2050516647524|
|35d40b482dd783be5...|          Visual Arts|  Grades 9-12|              32|null| 625.2050516647524| 625.2050516647524|
|8b176643eefb1a5ff...|          Visual Arts|  Grades 9-12|              40|null| 625.2050516647524| 625.2050516647524|
|0d3e005900a65100e...|          Visual Arts|  Grades 9-12|             100|null| 625.2050516647524| 625.2050516647524|
|c573c04a72ef2327c...|          Visual Arts|  Grades 9-12|              50|null| 625.2050516647524| 625.2050516647524|
|409bb022a9b6107e7...|          Visual Arts|  Grades 9-12|              44|null| 625.2050516647524| 625.2050516647524|
|bd9c5f8024d81b5d7...|          Visual Arts|  Grades 9-12|             133|null| 625.2050516647524| 625.2050516647524|
|b40eddf69c8aa5408...|          Visual Arts|  Grades 9-12|              25|null| 625.2050516647524| 625.2050516647524|
|bb70b5e0d78d710fd...|          Visual Arts|  Grades 9-12|              30|null| 625.2050516647524| 625.2050516647524|
|37883813cbe34bce4...|          Visual Arts|  Grades 9-12|              11|null| 625.2050516647524| 625.2050516647524|
|6c0d57362aa31b8fd...|          Visual Arts|  Grades 9-12|              25|null| 625.2050516647524| 625.2050516647524|
|2c206169d1f1ff5bd...|          Visual Arts|  Grades 9-12|               9|null| 625.2050516647524| 625.2050516647524|
|a6dbe5c98e0cd5ac7...|            Economics|Grades PreK-2|              75|null| 516.1365957446811| 516.1365957446811|
|fccc018812bc1cc8f...|            Economics|Grades PreK-2|             100|null| 516.1365957446811| 516.1365957446811|
|bad2cb1f19a9ec9fe...|            Economics|Grades PreK-2|              75|null| 516.1365957446811| 516.1365957446811|
|8d18dbbc31b93fbc4...|      Performing Arts|   Grades 3-5|              80|null| 567.4797188049205| 567.4797188049205|
|04db24864d6f96a59...|      Performing Arts|   Grades 3-5|             120|null| 567.4797188049205| 567.4797188049205|
|eaad74633cd57bdbf...|      Performing Arts|   Grades 3-5|              55|null| 567.4797188049205| 567.4797188049205|
|f261fccc594225afd...|      Performing Arts|   Grades 3-5|             500|null| 567.4797188049205| 567.4797188049205|
|c4db63c591935633a...|      Performing Arts|   Grades 3-5|              35|null| 567.4797188049205| 567.4797188049205|
|88ee865fae1c867d6...|      Performing Arts|   Grades 3-5|             500|null| 567.4797188049205| 567.4797188049205|
|99c87ae30865a60cd...|      Performing Arts|   Grades 3-5|              26|null| 567.4797188049205| 567.4797188049205|
|da42e29c3b847849a...|      Performing Arts|   Grades 3-5|              60|null| 567.4797188049205| 567.4797188049205|
|82b2945733995d850...|      Performing Arts|   Grades 3-5|              50|null| 567.4797188049205| 567.4797188049205|
|20203e6c80d45daa0...|      Performing Arts|   Grades 3-5|              81|null| 567.4797188049205| 567.4797188049205|
|816375fa721c90d6f...|      Performing Arts|   Grades 3-5|              60|null| 567.4797188049205| 567.4797188049205|
|9b0c7a1861bf01524...|      Performing Arts|   Grades 3-5|              15|null| 567.4797188049205| 567.4797188049205|
|25c1f08b8c6fcc0b0...|      Performing Arts|   Grades 3-5|             145|null| 567.4797188049205| 567.4797188049205|
|133b77f41185e7ac8...|      Performing Arts|   Grades 3-5|              64|null| 567.4797188049205| 567.4797188049205|
|bd3f69b5bc5f310af...|      Performing Arts|   Grades 3-5|              30|null| 567.4797188049205| 567.4797188049205|
|6efed44c08da5178f...|      Performing Arts|   Grades 3-5|             100|null| 567.4797188049205| 567.4797188049205|
|c2be8cea38597dcca...|      Performing Arts|   Grades 3-5|              31|null| 567.4797188049205| 567.4797188049205|
|b487636dfad8f2665...|      Performing Arts|   Grades 3-5|              50|null| 567.4797188049205| 567.4797188049205|
|2db51254d09dd7194...|      Performing Arts|   Grades 3-5|              93|null| 567.4797188049205| 567.4797188049205|
|4e08fb57c3e039ed6...|      Performing Arts|   Grades 3-5|              33|null| 567.4797188049205| 567.4797188049205|
|7a72e1a5024babe2b...|      Performing Arts|   Grades 3-5|              50|null| 567.4797188049205| 567.4797188049205|
|c93f4d35832bc8345...|      Performing Arts|   Grades 3-5|              62|null| 567.4797188049205| 567.4797188049205|
|f53649e8b3f834705...|      Performing Arts|   Grades 3-5|              50|null| 567.4797188049205| 567.4797188049205|
|b89f1fd459381c154...|      Performing Arts|   Grades 3-5|              75|null| 567.4797188049205| 567.4797188049205|
|9355abb61e5b34214...|      Performing Arts|   Grades 3-5|              25|null| 567.4797188049205| 567.4797188049205|
|12301a712b3b5c9e1...|      Performing Arts|   Grades 3-5|              75|null| 567.4797188049205| 567.4797188049205|
|8216575e01385e396...|      Performing Arts|   Grades 3-5|              30|null| 567.4797188049205| 567.4797188049205|
|f4a045454ec306b7e...|      Performing Arts|   Grades 3-5|             680|null| 567.4797188049205| 567.4797188049205|
|697b2b9c1092b0ab0...|      Performing Arts|   Grades 3-5|              97|null| 567.4797188049205| 567.4797188049205|
|4720f1a80614a97e0...|      Performing Arts|   Grades 3-5|              30|null| 567.4797188049205| 567.4797188049205|
|e79e07a502013fe15...|      Performing Arts|   Grades 3-5|              15|null| 567.4797188049205| 567.4797188049205|
|cc009ab3de8089ac4...|      Performing Arts|   Grades 3-5|              30|null| 567.4797188049205| 567.4797188049205|
|a99b55ebd86cd84db...|    Health & Wellness|  Grades 9-12|             700|null| 514.9477876106196| 514.9477876106196|
|51bc9c1a3957e9710...|    Health & Wellness|  Grades 9-12|              16|null| 514.9477876106196| 514.9477876106196|
|b8c3f2c0e9d6a41f6...|    Health & Wellness|  Grades 9-12|             200|null| 514.9477876106196| 514.9477876106196|
|5a1ea70d1ff30aca0...|    Health & Wellness|  Grades 9-12|              36|null| 514.9477876106196| 514.9477876106196|
|53db9bc944c64eba8...|    Health & Wellness|  Grades 9-12|              10|null| 514.9477876106196| 514.9477876106196|
|a850cbacca4a4548d...|  History & Geography|   Grades 3-5|             100|null|444.43908902691356|444.43908902691356|
|bcf69660b99a71e1f...|  History & Geography|   Grades 3-5|              75|null|444.43908902691356|444.43908902691356|
|2af93bb0209ec071d...|  History & Geography|   Grades 3-5|              14|null|444.43908902691356|444.43908902691356|
|8193ea5a48e6e9ae3...|  History & Geography|   Grades 3-5|              25|null|444.43908902691356|444.43908902691356|
|116dc5a3c1ff791b6...|  History & Geography|   Grades 3-5|              60|null|444.43908902691356|444.43908902691356|
|06ce0cb5547f6f909...|  History & Geography|   Grades 3-5|              58|null|444.43908902691356|444.43908902691356|
|76785e3669424f12c...|  History & Geography|   Grades 3-5|              87|null|444.43908902691356|444.43908902691356|
|b56bee5d8061b4f22...|  History & Geography|   Grades 3-5|              68|null|444.43908902691356|444.43908902691356|
|ba271e54cc7b09c40...|  History & Geography|   Grades 3-5|              50|null|444.43908902691356|444.43908902691356|
|198f9644b7527aadd...|  History & Geography|   Grades 3-5|              34|null|444.43908902691356|444.43908902691356|
|a55777f5be5672761...|  History & Geography|   Grades 3-5|              21|null|444.43908902691356|444.43908902691356|
|7b7ca891d0792aa56...|  History & Geography|   Grades 3-5|              29|null|444.43908902691356|444.43908902691356|
|2c600d453c596bf8c...|  History & Geography|   Grades 3-5|              88|null|444.43908902691356|444.43908902691356|
|2239a4e65f67daee6...|  History & Geography|   Grades 3-5|              71|null|444.43908902691356|444.43908902691356|
|106c12d17e32ae603...|  History & Geography|   Grades 3-5|              59|null|444.43908902691356|444.43908902691356|
|7452753b4c433617d...|  History & Geography|   Grades 3-5|              24|null|444.43908902691356|444.43908902691356|
|6a6584ab12bab7f3e...|  History & Geography|   Grades 3-5|              25|null|444.43908902691356|444.43908902691356|
|09d3dab8a89662101...|  History & Geography|   Grades 3-5|              31|null|444.43908902691356|444.43908902691356|
|8740cdaf46d227979...|  History & Geography|   Grades 3-5|              29|null|444.43908902691356|444.43908902691356|
|22991f3bf14d8fb03...|  History & Geography|   Grades 3-5|              32|null|444.43908902691356|444.43908902691356|
|2a38c94e33ae4626e...|  History & Geography|   Grades 3-5|              34|null|444.43908902691356|444.43908902691356|
|f2e16f70c83141559...|  History & Geography|   Grades 3-5|               5|null|444.43908902691356|444.43908902691356|
|7937488dd8f4c40ee...|  History & Geography|   Grades 3-5|              22|null|444.43908902691356|444.43908902691356|
|c81c0ad8cad12e10a...|  History & Geography|   Grades 3-5|              70|null|444.43908902691356|444.43908902691356|
|ec7389c75ba5d3baf...|  History & Geography|   Grades 3-5|              32|null|444.43908902691356|444.43908902691356|
|917530a53824c1a8c...|  History & Geography|   Grades 3-5|              60|null|444.43908902691356|444.43908902691356|
|ac2b684167b1b8155...|  History & Geography|   Grades 3-5|              32|null|444.43908902691356|444.43908902691356|
|ef06718f85fc8f2cc...|  History & Geography|   Grades 3-5|              10|null|444.43908902691356|444.43908902691356|
|52bf34ac1491f01b4...|  History & Geography|   Grades 3-5|              91|null|444.43908902691356|444.43908902691356|
|ba27a76bffe83d3df...|  History & Geography|   Grades 3-5|              99|null|444.43908902691356|444.43908902691356|
|519a10c507bcc6630...|  History & Geography|   Grades 3-5|             109|null|444.43908902691356|444.43908902691356|
|1d86fd3d645f025cd...|  History & Geography|   Grades 3-5|              29|null|444.43908902691356|444.43908902691356|
|e7f26152b93ca6c28...|  History & Geography|   Grades 3-5|              40|null|444.43908902691356|444.43908902691356|
|7f7ce695b51eb6585...|  History & Geography|   Grades 3-5|              28|null|444.43908902691356|444.43908902691356|
|8be08cf08a439930e...|  History & Geography|   Grades 3-5|             205|null|444.43908902691356|444.43908902691356|
|2aee9eba25d6fc410...|  History & Geography|   Grades 3-5|              99|null|444.43908902691356|444.43908902691356|
|2dac0eecbbd2f95c3...|  History & Geography|   Grades 3-5|               4|null|444.43908902691356|444.43908902691356|
|32ea7b5dd6898b60e...|  History & Geography|   Grades 3-5|             115|null|444.43908902691356|444.43908902691356|
|8e6f69d785ce30c93...|  History & Geography|   Grades 3-5|              55|null|444.43908902691356|444.43908902691356|
|b03449c926a4359f8...|  History & Geography|   Grades 3-5|              15|null|444.43908902691356|444.43908902691356|
|40921b4f520431bd4...|  History & Geography|   Grades 3-5|              40|null|444.43908902691356|444.43908902691356|
|f873a0b88869dfdda...|  History & Geography|   Grades 3-5|              43|null|444.43908902691356|444.43908902691356|
|4db779397c006ee89...|  History & Geography|   Grades 3-5|              83|null|444.43908902691356|444.43908902691356|
|e26fdf5d36d717c9f...|  History & Geography|   Grades 3-5|             135|null|444.43908902691356|444.43908902691356|
|0ad5a8f1b28e91c3e...|  History & Geography|   Grades 3-5|              30|null|444.43908902691356|444.43908902691356|
|3d187039501c8a61d...|  History & Geography|   Grades 3-5|             120|null|444.43908902691356|444.43908902691356|
+--------------------+---------------------+-------------+----------------+----+------------------+------------------+
only showing top 100 rows
 */