package donor.processing.file

import java.io.PrintWriter

import scala.io.Source

/**
  * some records are split on more then one line. the pattern specifies the record format. lines that not match the pattern are considered fragments
  * of a record and are merged together
  */
class BrokenLineMerger (inputFile:String,outputFile:String, pattern:String) {
  val in = Source.fromFile(inputFile, "UTF-8")
  val lineIterator = in.getLines()
  val out = new PrintWriter(outputFile)
  val regex = pattern.r
  var accumulator = new StringBuilder()

  def processGoodLine(resourceId:String, l: String) = {
    //flush accumulator
    out.println(accumulator.toString)
    //reset accumulator
    accumulator = new StringBuilder()
    //add new good line to accumulator
    accumulator.append(l)
  }

  def processBadLine(l: String) = {
    accumulator.append(l)
    println("Bad line appended:" + accumulator.toString)
  }

  def process() :Unit = {
    out.println(lineIterator.next) //skip header
    for (l <- lineIterator) {
      l match {
        case regex(resourceId,projectid) => processGoodLine(resourceId,l.trim)
        case _ => processBadLine(l.trim)
      }
    }
    out.println(accumulator.toString)
    out.close()
  }

}


object BrokenLineMerger {

  def processFile(inputFile:String,outputFile:String, pattern:String) = {
    val merger = new BrokenLineMerger(inputFile,outputFile,pattern)
    merger.process()
  }

  def main(args:Array[String]) = {
    //b7e35c5bf20f631613daceaed8c9940b,ffe609a94c749c5761058f782ce128fb,773,School Specialty,Supplies,ADJUSTABLE 3 HOLE PUNCH ADJUSTABLE 1/4'' SIZE 8 SHT CAP BK,1313918,6.17,1
    val pattern = "^((?:[0-9]|[a-z]){32}),((?:[0-9]|[a-z]){32}),.+$" //separator chars like \u2028 are not covered by .+, you should remove them first
    val dir = "data/input/"
    val inputFile = dir + "resources3.csv"
    val outputFile = dir + "resources4.csv"
    val merger = new BrokenLineMerger(inputFile, outputFile, pattern)

    merger.process()
  }
}
