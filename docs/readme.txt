Kaggle Competition:
https://www.kaggle.com/c/kdd-cup-2014-predicting-excitement-at-donors-choose

How to run:

1) Put original files from keggle in data/input directory. Input files are also availale in S3 bucket bytepocket-datascience
projects.csv
outcomes.csv
donations.csv
resources.csv
essay.csv

2) Some file are corrupted and need preprocessing.
Run DonorMain with parameter -proc
In S3 bucket are stored the preprocessed input files

*** Make Predicition with FG1 Features (first feature set, see DataRobot Entry Documentation) ***

3) Create FG1 Predictors by aggregation and historization:
Run DonorMain with parameter -pf

4) Create FG1 Response:
Run DonorMain with parameter -rf

5) In Python Project DonorNLP create word proxies from essay.csv.
Add wordproxies to FG1 predictors, by running DonorMain with parameter -wp

6) Export FG1 train and test set in csv format, so that they can be processed by R:
Run Donor main with parameter -tt

7) Make prediction by using FG1 train and test sets, by running script R/FG1Prediction.R
RScript  FG1Prediction.R
Store the prediction file in data/output/prediction_FG1.csv

*** Make Predicition with FG2 Features (first feature set, see DataRobot Entry Documentation) ***
8) Export data from essay.csv for natural language processing (NLP) in python project DonorNLP:
Run DonorMain with parameter -nlp.data
Produce stacked predictions in DonorNLP based on essay features

9) Export project data in order to be  processed in python project to do stacked prediction of cost with gbm and cv:
Run DonorMain with parameter -cost.data
Produce stacked prediction in DonorNLP based of cost

10) Assemble  FG2 train and test data by running DonorMain with parameter -fg2_tt

11) Make prediction by using FG2 train and test sets, by running script R/FG2Prediction.R
RScript  FG2Prediction.R
Store the prediction file in data/output/prediction_FG2.csv

12) Make final prediction by averaging FG1 and FG2 predictions.
Run DonorMain with parameter -final
Submit prediction to Kaggle








